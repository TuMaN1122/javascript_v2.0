// Task 1 ============================================
/* Дан input .i-1. Напишите функцию t1, которая выводит в .out-1 символ и возвращает его. Во всех последующих задачах - работаем с латиницей и цифрами.*/
// let key1= 
function t1(event) {
    return document.querySelector('.out-1').textContent += event.key;
}
document.querySelector('.i-1').onkeypress = t1;
// ваше событие здесь!!!

// Task 2 ============================================
/*  Дан input .i-2. Напишите функцию t2, которая выводит в .out-2 код символа и возвращает его. */
let i2 = document.querySelector('.i-2');
let out2 = document.querySelector('.out-2');

function t2() {
    i2.value = '';
    out2.textContent = '';
    return out2.textContent += event.keyCode;
}
i2.onkeypress = t2;

// ваше событие здесь!!!


// Task 3 ============================================
/*  Дан input .i-3. Напишите функцию t3, которая выводит на страницу true если введен символ и false если цифра. Для определения - используйте код клавиши. */
let i3 = document.querySelector('.i-3');
let out3 = document.querySelector('.out-3');

function t3() {
    i3.value = '';
    out3.textContent = '';
    if (event.keyCode < 48 || event.keyCode > 57) {
        out3.textContent = true;
    } else {
        out3.textContent = false;
    }
}
i3.onkeypress = t3;

// ваше событие здесь!!!


// Task 4 ============================================
/*  Дан input .i-4. Напишите функцию t4, которая выводит в .out-4 только символы в нижнем регистре. Т.е. ввели ab4Bci в out получаем ab4bci. */
let i4 = document.querySelector('.i-4');
let out4 = document.querySelector('.out-4');

function t4() {
    out4.textContent += event.key.toLowerCase();
}
i4.onkeypress = t4;

// ваше событие здесь!!!

// Task 5 ============================================
/*  Дан input .i-5. Напишите функцию t5, которая выводит в .out-5 все вводимые символы в верхнем регистре. Т.е. пользователь ввел AbCd и функция выведет ABCD. */

let i5 = document.querySelector('.i-5');
let out5 = document.querySelector('.out-5');

function t5() {
    out5.textContent += event.key.toUpperCase();
}
i5.onkeypress = t5;

// ваше событие здесь!!!

// Task 6 ============================================
/*  Дан input .i-6. Напишите функцию t6, которая выводит в .i-6 только символы в нижнем регистре.  */

let i6 = document.querySelector('.i-6');

function t6() {
    return i6.value = i6.value.toLowerCase();
}
i6.oninput = t6;

// ваше событие здесь!!!


// Task 7 ============================================
/*  Дан input .i-7. Напишите функцию t7, которая выводит в .out-7 случаный символ из массива a7 при каждом вводе символа. */

let i7 = document.querySelector('.i-7');
let out7 = document.querySelector('.out-7');

function t7() {
    const a7 = ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o'];

    function randomItem(min, max) {
        let rand = min + Math.random() * (max + 1 - min);
        return Math.floor(rand);
    }
    out7.textContent += a7[randomItem(0, a7.length - 1)];
}
i7.onkeypress = t7;

// ваше событие здесь!!!

// Task 8 ============================================
/*  Дан input .i-8. Напишите функцию t8, которая выводит в .out-8 вводимый в input текст, но заменяет i на 1, o на 0, l на 7. */

let i8 = document.querySelector('.i-8');
let out8 = document.querySelector('.out-8');

function t8() {
    switch (event.key) {
        case 'i':
            out8.textContent += 1;
            break;
        case 'o':
            out8.textContent += 0;
            break;
        case 'l':
            out8.textContent += 7;
            break;
        default:
            out8.textContent += event.key;
            break;
    }
}
i8.onkeypress = t8;
// ваше событие здесь!!!


// Task 9 ============================================
/* Дан input .i-9. Напишите функцию t8, выводит в .out-9 количество нажатых клавиш стрелка вниз. */

let i9 = document.querySelector('.i-9');
let out9 = document.querySelector('.out-9');

let num = 0;

function t9() {
    if (event.key === 'ArrowDown') {
        num++;
    }
    out9.innerHTML = num;
}
i9.addEventListener('keyup', t9)
// i9.onkeyup = t9;
// ваше событие здесь!!!


// Task 10 ============================================
/*  Дан input .i-10 и изображение 1.png. Добавьте событие на input, при нажатии клавиш стрелка вправо и стрелка влево увеличивать ширину изображения. Клавиши стрелка вверх и вниз - увеличивать высоту изображения. Одно нажатие клавиши - 1px. */
// lr 37 39
// 38 40
let i10 = document.querySelector('.i-10'),
    w10 = document.querySelector('.block').offsetWidth,
    h10 = document.querySelector('.block').offsetHeight,
    img = document.querySelector('img'),
    block = document.querySelector('.block');

function t10() {
    if (event.keyCode === 37 || event.keyCode === 39) {
        img.style.width = w10 + 'px';
        block.style.width = w10 + 'px';
        w10 += 1;
    };
    if (event.keyCode === 38 || event.keyCode === 40) {
        img.style.height = h10 + 'px';
        block.style.height = h10 + 'px';
        h10 += 1;
    };
}
i10.onkeyup = t10;

// ваше событие здесь!!!

// Task 11 ============================================
/*  Проект. Дан input .i-11. Используя знания html и css нарисуйте клавиатуру (можно схематически). Изображение должно содержать числа, символьные клавиши, пробел, enter, caps lock, shift, tab, alt. При вводе текста в input в момент нажатия клавиши - затемняйте ее, в момент отпускания - возвращайте к первоначальному состоянию. Аналогично при нажатии enter, space, alt, shift, ctrl. Затемнение реализуйте через добавление класса CSS. Для удобства рекомендую каждой клавише добавить атрибут data с символом. Если нажата клавиша caps lock - то присвоить ей затемнение, которое работает до последующего отжатия клавиши. */
let i11 = document.querySelector('.i-11');
let key11 = document.querySelectorAll('[data-key]')

function t11(event) {
    key11.forEach(item => {
        if (event.code === item.dataset.key || event.code === item.dataset.key.toUpperCase()) {
            item.classList.toggle('hover');
            this.onkeyup = (event) => {
                key11.forEach(item => {
                    if (event.code === item.dataset.key || event.code === item.dataset.key.toUpperCase()) {
                        item.classList.remove('hover');
                    }
                });
            };
        }
    });
}
i11.onkeydown = t11;
// i11.onkeyup = t11;
// ваше событие здесь!!!