//  Task 1
// <p>С помощью вложенных циклов, нарисуйте строку:</p>
// <pre>
// ***_***_***_
// </pre>
// <p>где звездочки рисуются с помощью внутреннего цикла от 0 до 3, а _ с помощью внешнего.</p>
let out1 = document.querySelector('.out-1');
function t1() {
    for(let i = 0; i < 3; i++) {
        for(let k = 0; k < 3; k++) {
            out1.innerHTML += '*';
        }
        out1.innerHTML += '_';
    }
}
document.querySelector('.b-1').onclick = t1;



//  Task 2
// <p>С помощью вложенных циклов, нарисуйте строку:</p>
// <pre>
// 1
// *_*_*_
// 2
// *_*_*_
// 3
// *_*_*_
// </pre>
// <p>Решить задачу с помощью вложенных циклов. Внешний цикл выводит цифры и перенос строки br, , второй звездочки, знак подчеркивания и знак переноса.</p>
let out2 = document.querySelector('.out-2');
function t2() {
    for(let i = 0; i < 3; i++) {
        out2.innerHTML += `${i+1} <br>`;
        for(let k = 0; k < 3; k++) {
            out2.innerHTML += '*_';
        }
        out2.innerHTML += '<br>';
    }
}

document.querySelector('.b-2').onclick = t2;



//  Task 3
// <p>С помощью вложенных циклов, нарисуйте строку:</p>
// <pre>
// *_*_*_
// *_*_*_
// *_*_*_
// *_*_*_
// </pre>
// <p>Решить задачу с помощью вложенных циклов. Внешний цикл выводит перенос строки br,  внутренний -  звездочки, знак подчеркивания.</p>
let out3 = document.querySelector('.out-3');
function t3() {
    for(let i = 0; i < 3; i++) {
        for(let k = 0; k < 3; k++) {
            out3.innerHTML += `*_`;
        }
        out3.innerHTML += `<br>`;
    }
}

document.querySelector('.b-3').onclick = t3;



//  Task 4
// <p>С помощью вложенных циклов, нарисуйте строку:</p>
// <pre>
// 0_10_1_9_2_8_3_7_4_6_5_5_6_4_7_3_8_2_9_1_10_0_
// </pre>
// <p>Решить задачу с помощью вложенных циклов. Внешний цикл выводит числа на четных позициях (от 0 до 10) внутренний цикл  - числа на нечетных позициях  (от 10 до 0).</p>
let out4 = document.querySelector('.out-4');
function t4() {
    let n = 10;
    for (let i = 0; i <= 10; i++) { 
        out4.innerHTML += `${i}_`;
      for (let k = 0; k < 1; k++) {
        out4.innerHTML += `${n--}_`;
      }
    }
}

document.querySelector('.b-4').onclick = t4;



//  Task 5
// <p>С помощью вложенных циклов, нарисуйте строку:</p>
// <pre>
// 101010
// 101010
// 101010
// </pre>
// <p>Внешний цикл выводит перенос строки br. Вложенный цикл от 0 до 6 выводит либо 0 либо 1.</p>
let out5 = document.querySelector('.out-5');
function t5() {
    for (let i = 0; i < 3; i++) { 
        for (let k = 0; k < 6; k++) {
            if(k % 2 == 0) {
                out5.innerHTML += 1;
            } else {
                out5.innerHTML += 0;
            }
        }
        out5.innerHTML += `<br>`;
    }
}

document.querySelector('.b-5').onclick = t5;



//  Task 6
// <p>С помощью вложенных циклов, нарисуйте строку:</p>
// <pre>
// 10x01x
// 10x01x
// 10x01x
// </pre>
// <p>Внешний цикл выводит перенос строки br. Вложенный цикл от 0 до 6 выводит либо 0 либо 1 либо х.</p>
let out6 = document.querySelector('.out-6');
function t6() {
    for (let i = 0; i < 3; i++) { 
        for (let k = 1; k <= 6; k++) {
            if(k % 2 == 0 && k % 3 != 0) {
                out6.innerHTML += 0;
            } else if(k % 3 === 0) {
                out6.innerHTML += 'x';
            } else {
                out6.innerHTML += 1;
            }
        }
        out6.innerHTML += `<br>`;
    }
}

document.querySelector('.b-6').onclick = t6;



//  Task 7
// <p>С помощью вложенных циклов, нарисуйте строку:</p>
// <pre>
// *
// **
// ***
// ****
// </pre>
// <p>Внешний цикл выводит перенос строки br. Вложенный цикл запускается от нуля до i и рисует звездочку.</p>
function t7() {
    let out7 = document.querySelector('.out-7');
    for (let i = 0; i < 5; i++) { 
        for (let k = 0; k < i; k++) {
            out7.innerHTML += `*`;
        }
        out7.innerHTML += `<br>`;
    }
}

document.querySelector('.b-7').onclick = t7;



//  Task 8
// <p>С помощью вложенных циклов, нарисуйте строку:</p>
// <pre>
// *****
// ****
// ***
// **
// *
// </pre>
// <p>Внешний цикл выводит перенос строки br. Вложенный цикл рисует звездочки. </p>
function t8() {
    let out8 = document.querySelector('.out-8');
    for (let i = 5; i >= 0; i--) { 
        for (let k = 0; k < i; k++) {
            out8.innerHTML += `*`;
        }
        out8.innerHTML += `<br>`;
    }
}

document.querySelector('.b-8').onclick = t8;



//  Task 9
// <p>С помощью вложенных циклов, нарисуйте строку:</p>
// <pre>
// 1
// 1 2
// 1 2 3
// 1 2 3 4
// 1 2 3 4 5
// </pre>
// <p>Внешний цикл выводит перенос строки br. Вложенный цикл рисует цифры</p>
function t9() {
    let out9 = document.querySelector('.out-9');
    for (let i = 1; i <= 5; i++) { 
        for (let k = 0; k < i; k++) {
            out9.innerHTML += `${k + 1}  `;
        }
        out9.innerHTML += `<br>`;
    }
}

document.querySelector('.b-9').onclick = t9;



//  Task 10
// <p>С помощью вложенных циклов, нарисуйте строку:</p>
// <pre>
// 01 02 03 04 05 06 07 08 09 10
// 11 12 13 14 15 16 17 18 19 20
// 21 22 23 24 25 26 27 28 29 30
// 31 32 33 34 35 36 37 38 39 40
// 41 42 43 44 45 46 47 48 49 50
// </pre>
// <p>Внешний цикл выводит перенос строки br и запускается от 0 до 6.</p>
// <p>Вложенный цикл рисует цифры от 0 до 9. Обратите внимание, что первый ряд - есть ведущий нуль. Здесь все просто - проверили, если число меньше 10 - то конкатенируем нуль.</p>
function t10() {
    let out10 = document.querySelector('.out-10');
    for (let i = 0; i < 5; i++) { 
        for (let k = 0; k <= 9; k++) {
           if(i == 0 && k < 9) {
            out10.innerHTML += `0${k + 1}  `;
           } else if(k == 9){
            out10.innerHTML += `${i + 1}0  `;
           } else {
            out10.innerHTML += `${i}${k + 1}  `;
           }
        }
        out10.innerHTML += `<br>`;
    }
}

document.querySelector('.b-10').onclick = t10;



//====================Задачи на прокачку====================

// Task 1.
// С помощью вложенных циклов и символа * нарисуйте:
//    *****
//    *****
//    *****
function t11() {
    let out11 = document.querySelector('.out-11');
    for(let i = 0; i < 3; i++) {
        for(let k = 0; k < 5; k++) {
            out11.innerHTML += '*';
        }
        out11.innerHTML += '<br>';
    }
}

document.querySelector('.b-11').onclick = t11;



// Task 2
// С помощью вложенных циклов и символа 1,0 нарисуйте прямоугольник. 1 или 0 выводите в зависимости от того четная или нет переменная внутреннего цикла.
//    101010
//    101010
//    101010
function t12() {
    let out12 = document.querySelector('.out-12');
    for (let i = 0; i < 3; i++) { 
        for (let k = 0; k < 6; k++) {
            if(k % 2 == 0) {
                out12.innerHTML += 1;
            } else {
                out12.innerHTML += 0;
            }
        }
        out12.innerHTML += `<br>`;
    }
}

document.querySelector('.b-12').onclick = t12;


// Task 3
// С помощью вложенных циклов и символа 1,0 нарисуйте прямоугольник. 1 или 0 выводите в зависимости от того четная или нет переменная внутреннего цикла. Каждый третий элемент заменяйте на x:
// 10x01x
// 10x01x
// 10x01x
function t13() {
    let out13 = document.querySelector('.out-13');
    for (let i = 0; i < 3; i++) { 
        for (let k = 1; k <= 6; k++) {
            if(k % 2 == 0 && k % 3 != 0) {
                out13.innerHTML += 0;
            } else if(k % 3 === 0) {
                out13.innerHTML += 'x';
            } else {
                out13.innerHTML += 1;
            }
        }
        out13.innerHTML += `<br>`;
    }
}

document.querySelector('.b-13').onclick = t13;


// Task 4
// С помощью вложенных циклов и символа * нарисуйте:
// *
// **
// ***
function t14() {
    let out14 = document.querySelector('.out-14');
    for(let i = 0; i < 4; i++) {
        for(let k = 0; k < i; k++) {
            out14.innerHTML += '*';
        }
        out14.innerHTML += '<br>';
    }
}

document.querySelector('.b-14').onclick = t14;



// Task 5
// С помощью вложенных циклов и символа * нарисуйте:
// *****
// ****
// ***
// **
// *
function t15() {
    let out15 = document.querySelector('.out-15');
    for (let i = 5; i >= 0; i--) { 
        for (let k = 0; k < i; k++) {
            out15.innerHTML += `*`;
        }
        out15.innerHTML += `<br>`;
    }
}

document.querySelector('.b-15').onclick = t15;



// Task 6
// С помощью вложенных циклов и символа * нарисуйте:
//   *****
//  *****
// *****
function t16() {
    let out16 = document.querySelector('.out-16');
    for (let i = 3; i > 0; i--) {
        for (let k = i; k > 0; k--) {
            out16.innerHTML += `&nbsp; `;
        }
        out16.innerHTML += `******<br>`;
    }
}

document.querySelector('.b-16').onclick = t16;



// Task 7
// С помощью вложенных циклов и символа * нарисуйте:
// *
// **
// ***
// **
// *

// function t17() {
//     let out17 = document.querySelector('.out-17');
//     let num = 0;
//     for (let i = 0; i < 6; i++) {
//         if (i < 3){
//             num++;
//             for (let k = 0; k < num; k++) {
//                 out17.innerHTML += '*';
//             }
//         } else if (i >= 3 && i <= 7) {
//             num--;
//             for (let k = 0; k < num; k++) {
//                 out17.innerHTML += '*';
//             }
//         }
//         out17.innerHTML += '<br>';
//     }
// }
// document.querySelector('.b-17').onclick = t17;

function t17() {
    let out17 = document.querySelector('.out-17');
    for (let i = 1; i <= 3; i++) {
        for (let k = 1; k <= i; k++) {
            out17.innerHTML += '*';
        }
        out17.innerHTML += '<br>';
    }
    for (let i = 1; i >= 0; i--){
        for (let k = 2; k >= 2 - i; k--){
            out17.innerHTML += '*';
        }
    out17.innerHTML += '<br>';
    }
}

document.querySelector('.b-17').onclick = t17;




// Task 8
// С помощью вложенных циклов и символа * нарисуйте:
// ******
// *    *
// *    *
// *    *
// ******
function t18() {
    let out18 = document.querySelector('.out-18');
    for (let i = 0; i < 5; ++i) {
        if (i == 0 || i == 4) {    
          for (let k = 0; k < 6; k++) {
            out18.innerHTML += '*';
          }    
        } else {
            out18.innerHTML += '*';
          for (let k = 0; k < 6 ; k++) {
            out18.innerHTML += '&nbsp;';
          }    
          out18.innerHTML += '*';
        }   
        out18.innerHTML += '<br>';
    }
}

document.querySelector('.b-18').onclick = t18;



// Task 9
// С помощью вложенных циклов и символа который вводит пользователь нарисуйте:
// ******
// *    *
// *    *
// *    *
// ******
function t19() {  
    let x = prompt('введите любой символ', '*');
    console.log(x)
    let out19 = document.querySelector('.out-19');
    if(x !== null ) {
        for (let i = 0; i < 5; ++i) {
            if (i == 0 || i == 4) {    
              for (let k = 0; k < 6; k++) {
                out19.innerHTML += x;
              }    
            } else {
                out19.innerHTML += x;
              for (let k = 0; k < 6 ; k++) {
                out19.innerHTML += '&nbsp;';
              }    
              out19.innerHTML += x;
            }   
            out19.innerHTML += '<br>';
        }
    } else {
        alert('Вы не выпонели условие');
    }
}

document.querySelector('.b-19').onclick = t19;



// Task 10
// С помощью вложенных циклов вывените таблицу умножения на 6 и 7
function t20() {
    let out20 = document.querySelector('.out-20');
    for(let i = 6; i <= 7; i++) {
        for(let k = 1; k < 10; k++) {
            out20.innerHTML += `<span>${i} * ${k} = ${i * k}</span>  `
        }
        out20.innerHTML += '<br>'
    }
}

document.querySelector('.b-20').onclick = t20;


// Task 11
// С помощью вложенных циклов нарисуйте, цифры - из счетчиков цикла:
// 1
// 12
// 123
// 1234
// 12345
function t21() {
    let out21 = document.querySelector('.out-21');
    for (let i = 1; i <= 6; i++) { 
        for (let k = 1; k < i; k++) {
            out21.innerHTML += `${k}  `;
        }
        out21.innerHTML += `<br>`;
    }
}

document.querySelector('.b-21').onclick = t21;



// Task 12.
// С помощью вложенных циклов нарисуйте, цифры - из счетчиков цикла:
// 01 02 03 04 05 06 07 08 09 10
// 11 12 13 14 15 16 17 18 19 20
// 21 22 23 24 25 26 27 28 29 30
// 31 32 33 34 35 36 37 38 39 40
// 41 42 43 44 45 46 47 48 49 50
function t22() {
    let out22 = document.querySelector('.out-22');
    for (let i = 0; i < 5; i++) { 
        for (let k = 0; k <= 9; k++) {
           if(i == 0 && k < 9) {
            out22.innerHTML += `0${k + 1}  `;
           } else if(k == 9){
            out22.innerHTML += `${i + 1}0  `;
           } else {
            out22.innerHTML += `${i}${k + 1}  `;
           }
        }
        out22.innerHTML += `<br>`;
    }
}

document.querySelector('.b-22').onclick = t22;



// Task 13
// С помощью вложенных циклов нарисуйте, цифры - из счетчиков цикла:
// 54321
// 4321
// 321
// 21
// 1
function t23() {
    let out23 = document.querySelector('.out-23');
    for(let i = 5; i > 0; i--) {
        for(let k = i; k> 0; k--) {
            out23.innerHTML += k;
        }
        out23.innerHTML += '<br>';
    }
}

document.querySelector('.b-23').onclick = t23;



// Task 14
// С помощью вложенных циклов нарисуйте, цифры - из счетчиков цикла:
//     1
//    12
//   123
//  1234
// 12345
function t24() {
    let out24 = document.querySelector('.out-24');
    for (let i = 1; i < 6; i++) { 
        for (let k = 6; k >= i; k--) {
            out24.innerHTML += `&nbsp;&nbsp;`;
        }
        for (let j = 0; j < i; j++) {
            out24.innerHTML += j+1;
        }
        out24.innerHTML += `<br>`;
    }
}

document.querySelector('.b-24').onclick = t24;



// Task 15
// С помощью вложенных циклов нарисуйте, цифры - из счетчиков цикла:
// X X X X 1
// X X X 2 1
// X X 3 2 1
// X 4 3 2 1
// 5 4 3 2 1
function t25() {
    let out25 = document.querySelector('.out-25');
    for (let i = 2; i < 7; i++) {
        for (k = 5; k >= 1; k--) {
            if (k < i) {
                out25.innerHTML += `${k} `;
            }
            else {
                out25.innerHTML += `x `;
            }
        }
        out25.innerHTML += `<br>`;
    }
}

document.querySelector('.b-25').onclick = t25;



// Task 16
// С помощью вложенных циклов нарисуйте, цифры - из счетчиков цикла:
// 1
// 2 2
// 3 3 3
// 4 4 4 4
// 5 5 5 5 5
function t26() {
    let out26 = document.querySelector('.out-26');
    for(let i = 0; i <= 5; i++) {
        for(let k = 0; k < i; k++) {
            out26.innerHTML += `${i}  `;
        }
        out26.innerHTML += '<br>';
    }
}

document.querySelector('.b-26').onclick = t26;



// Task 17
// С помощью вложенных циклов нарисуйте, цифры - из счетчиков цикла:
// 5
// 4 4
// 3 3 3
// 2 2 2 2
// 1 1 1 1 1
function t27() {
    let out27 = document.querySelector('.out-27');
    for(let i = 0; i <= 5; i++) {
        let num = 6;
        for(let k = 0; k < i; k++) {
            out27.innerHTML += `${num - i}  `;
        }
        out27.innerHTML += '<br>';
    }
}

document.querySelector('.b-27').onclick = t27;



// Task 18
// С помощью вложенных циклов нарисуйте, цифры - из счетчиков цикла (четные заменены на X):
// 5
// x x
// 3 3 3
// x x x x
// 1 1 1 1 1
function t28() {
    let out28 = document.querySelector('.out-28');
    for(let i = 0; i <= 5; i++) {
        let num = 6;
        for(let k = 0; k < i; k++) {
            if(i % 2 != 0) {
                out28.innerHTML += `${num - i}  `;
            } else {
                out28.innerHTML += `x `;
            }
        }
        out28.innerHTML += '<br>';
    }
}

document.querySelector('.b-28').onclick = t28;



// Task 19
// С помощью вложенных циклов и символа * нарисуйте:
//   *****
//  *******
// *********
function t29() {
    let out29 = document.querySelector('.out-29');
    for (let i = 1; i < 4; i++) {
        for (k = 5; k >= 1; k--) {
            if (k < i) {
                out29.innerHTML += `**`;
            }
            else {
                out29.innerHTML += `&nbsp;`;
            }
        }
        out29.innerHTML += `*****`
        out29.innerHTML += `<br>`;
    }
}

document.querySelector('.b-29').onclick = t29;



// Task 20
// С помощью вложенных циклов и символа * нарисуйте:
//   **
//  ****
// ******
//  ****
//   **
function t30() {
    let out30 = document.querySelector('.out-30');
    for (let i = 1; i <= 3; i++) {
        for (let k = 3; k > i; k--) {
            out30.innerHTML += `&nbsp;`;
        }
        for (let j = 0; j < i; j++) {
            out30.innerHTML += '**';
        }
        out30.innerHTML += '<br>';
    }
    for (let i = 3; i > 1; i--) {
        for (let k = 3; k >= i; k--) {
            out30.innerHTML += `&nbsp;`;
        }
        for (let j = 1; j < i; j++) {
            out30.innerHTML += '**';
        }
        out30.innerHTML += '<br>';
    }
}

document.querySelector('.b-30').onclick = t30;