let btn = document.querySelector('.location__btn'),
	weatherSelect = document.querySelector('.weather__select'),
	weatherDate = document.querySelector('.weather__location--date'),
	weatherSity = document.querySelector('.weather__location--city'),
	selectRegion = document.querySelector('select[name=region]'),
	selectSity = document.querySelector('select[name=sity]'),
	temp = document.querySelector('.weather__details__temp'),
	tempRang = document.querySelector('.temp__range'),
	humidity = document.querySelector('.humidity'),
	pressure = document.querySelector('.pressure'),
	wind = document.querySelector('.wind'),
	img = document.querySelector('.weather__details img'),
	fewdaysDate = document.querySelectorAll('.fewdays__date'),
	fewdaysImg = document.querySelectorAll('.weather__fewdays__item img'),
	d = new Date(),
	day = new Array("Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"),
	month = new Array("января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря");


weatherDate.innerHTML = `${day[d.getDay()]}. ${d.getDate()} ${month[d.getMonth()]} ${d.getFullYear()}г.`;


for (let i = 0; i < fewdaysDate.length; i++) {
	let k = 1;
	k += i;
	fewdaysDate[i].innerHTML = `${d.getDate() + k} ${month[d.getMonth()]} ${d.getFullYear()}г.`;
}

function openSelect() {
	if (weatherSelect.style.maxHeight) {
		weatherSelect.style.maxHeight = null;
	} else {
		weatherSelect.style.maxHeight = weatherSelect.scrollHeight + "px";
	}
}
btn.onclick = openSelect;

const region = {
	'Брестская область': {
		'Барановичи': {
			"id": 630429,
			"name": "Baranovichi",
		},
		'Берёза': {
			"id": 630206,
			"name": "Berëza",
		},
		'Брест': {
			"id": 629634,
			"name": "Brest",
		},
		'Ивацевичи': {
			"id": 627800,
			"name": "Ivatsevichy",
		},
		'Кобрин': {
			"id": 627145,
			"name": "Kobryn",
		},
		'Пинск': {
			"id": 623549,
			"name": "Pinsk",
		},
		'Пружаны': {
			"id": 622997,
			"name": "Pruzhany",
		},
		'Столин': {
			"id": 621277,
			"name": "Stolin",
		},
	},
	'Витебская область': {
		'Витебск': {
			"id": 620127,
			"name": "Vitebsk",
		},
		'Орша': {
			"id": 624079,
			"name": "Orsha",
		},
		'Полоцк': {
			"id": 623317,
			"name": "Polatsk",
		},
		'Ушачи': {
			"id": 620540,
			"name": "Ushachy",
		},
	},
	'Гомельская область': {
		'Гомель': {
			"id": 627907,
			"name": "Homyel'",
		},
		'Добруш': {
			"id": 629018,
			"name": "Dobrush",
		},
		'Туров': {
			"id": 620676,
			"name": "Turaw",
		},
		'Мозырь': {
			"id": 624965,
			"name": "Mosar",
		},
		'Речица': {
			"id": 622794,
			"name": "Rechytsa",
		},
		'Рогачев': {
			"id": 622739,
			"name": "Rahachow",
		},
	},
	'Гродненская область': {
		'Волковыск': {
			"id": 620391,
			"name": "Volkovysk",
		},
		'Гродно': {
			"id": 628035,
			"name": "Grodno",
		},
		'Лида': {
			"id": 626081,
			"name": "Lida",
		},
		'Мосты': {
			"id": 625367,
			"name": "Mosty",
		},
		'Ошмяны': {
			"id": 630515,
			"name": "Ashmyany",
		},
		'Островец': {
			"id": 623941,
			"name": "Astravyets",
		},
		'Слоним': {
			"id": 621754,
			"name": "Slonim",
		},
	},
	'Минская область': {
		'Березино': {
			"id": 630197,
			"name": "Byerazino",
		},
		'Борисов': {
			"id": 630376,
			"name": "Barysaw",
		},
		'Вилейка': {
			"id": 620181,
			"name": "Vilyeyka",
		},
		'Воложин': {
			"id": 620445,
			"name": "Valozhyn",
		},
		'Дзержинск': {
			"id": 628634,
			"name": "Dzyarzhynsk",
		},
		'Клецк': {
			"id": 627214,
			"name": "Klyetsk",
		},
		'Логойск': {
			"id": 625972,
			"name": "Lahoysk",
		},
		'Минск': {
			"id": 625144,
			"name": "Minsk",
		},
		'Мядель': {
			"id": 624861,
			"name": "Myadzyel",
		},
		'Несвиж': {
			"id": 624700,
			"name": "Nyasvizh",
		},
		'Слуцк': {
			"id": 621741,
			"name": "Slutsk",
		},
		'Солигорск': {
			"id": 622428,
			"name": "Salihorsk",
		},
		'Узда': {
			"id": 620483,
			"name": "Uzda",
		},
		'Червень': {
			"id": 629273,
			"name": "Chervyen’",
		},
	},
	'Могилёвская область': {
		'Бобруйск': {
			"id": 630468,
			"name": "Babruysk",
		},
		'Быков': {
			"id": 629447,
			"name": "Bykhaw",
		},
		'Климовичи': {
			"id": 627202,
			"name": "Klimavichy",
		},
		'Кричев': {
			"id": 626450,
			"name": "Krychaw",
		},
		'Могилев': {
			"id": 625665,
			"name": "Mahilyow",
		},
		'Осиповичи': {
			"id": 624034,
			"name": "Asipovichy",
		},
		'Шклов': {
			"id": 622034,
			"name": "Shklow",
		},
	},
}

for (let key in region) {
	selectRegion.innerHTML += `<option value="${key}">${key}</option>`;
}

selectRegion.onchange = () => {
	selectSity.innerHTML = '<option value="0" selected>Выберите город</option>';
	for (let key in region) {
		if (selectRegion.value == key) {
			for (let i in region[key]) {
				selectSity.innerHTML += `<option value="${i}">${i}</option>`;
			}
		}
	}
}
selectSity.onchange = () => {
	if (selectSity.value) {
		openSelect();
	}
	weatherSity.innerHTML = `${selectSity.value} BY`;
	// console.log(region[selectRegion.value][selectSity.value]['id'])
	fetch(`http://api.openweathermap.org/data/2.5/weather?id=${region[selectRegion.value][selectSity.value]['id']}&units=metric&appid=4ba3f239e810a271ee479bcd74d8d3e6`)
		.then(function (resp) {
			return resp.json()
		})
		.then(function (data) {
			// console.log(data);
			temp.innerHTML = `${Math.round(data.main.temp)}&deg;C `;
			tempRang.innerHTML = `Температура мин/макс: ${Math.round(data.main.temp_min)} / ${Math.round(data.main.temp_max)}&deg;C`
			humidity.innerHTML = `Влажность: ${data.main.humidity}%`;
			pressure.innerHTML = `Давление: ${data.main.pressure}гПа`;
			wind.innerHTML = `Скорость ветра: ${data.wind.speed}м/с`;
			img.src = `https://openweathermap.org/img/wn/${data.weather[0]['icon']}@2x.png`;


			for (let i = 0; i < fewdaysImg.length; i++) {
				fewdaysImg[i].src = `https://openweathermap.org/img/wn/${data.weather[0]['icon']}@2x.png`;
			}
		})
		.catch(function () {});
}