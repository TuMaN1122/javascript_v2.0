{
	"id": 618716,
	"name": "Zolotva",
	"country": "BY"

}

{
	"id": 618768,
	"name": "Zhukovo",
	"country": "BY"

} {
	"id": 618781,
	"name": "Zhuki",
	"country": "BY"
} {
	"id": 618800,
	"name": "Horad Zhodzina",
	"country": "BY"
} {
	"id": 618806,
	"name": "Zhlobin",
	"country": "BY"
} {
	"id": 618815,
	"name": "Zhytkavichy",
	"country": "BY"
} {
	"id": 618816,
	"name": "Zhitin",
	"country": "BY"
} {
	"id": 618818,
	"name": "Zhirovichi",
	"country": "BY"
} {
	"id": 618835,
	"name": "Zhydachiv",
	"country": "UA",
} {
	"id": 618883,
	"name": "Zhdanovichy",
	"country": "BY"

} {
	"id": 618890,
	"name": "Zhaulki",
	"country": "BY"

} {
	"id": 618913,
	"name": "Zhabinka",
	"country": "BY"

} {
	"id": 618927,
	"name": "Zyembin",
	"country": "BY"

} {
	"id": 618965,
	"name": "Zbyshin",
	"country": "BY"
} {
	"id": 618982,
	"name": "Zaytsevo",
	"country": "BY"
} {
	"id": 619015,
	"name": "Zavitaya",
	"country": "BY"

} {
	"id": 619023,
	"name": "Zavershany",
	"country": "BY"
} {
	"id": 619034,
	"name": "Zatsen’",
	"country": "BY"
} {
	"id": 619066,
	"name": "Zaslawye",
	"country": "BY"
} {
	"id": 619076,
	"name": "Zasyetstsye",
	"country": "BY"
} {
	"id": 619095,
	"name": "Zarovtsy",
	"country": "BY"

} {
	"id": 619204,
	"name": "Zamostsye",
	"country": "BY"

} {
	"id": 619217,
	"name": "Zamostochye",
	"country": "BY"
} {
	"id": 619258,
	"name": "Zalozy",
	"country": "BY"

} {
	"id": 619340,
	"name": "Zagor’ye",
	"country": "BY"

}

{
	"id": 619536,
	"name": "Yurevichi",
	"country": "BY"

}

{
	"id": 619594,
	"name": "Yeremeyevshchina",
	"country": "BY"

}

{
	"id": 619596,
	"name": "Yemel’yanovo",
	"country": "BY"

}

{
	"id": 619598,
	"name": "Yel’sk",
	"country": "BY"

}

{
	"id": 619616,
	"name": "Yalizava",
	"country": "BY"
}

{
	"id": 619670,
	"name": "Yasnyy Les",
	"country": "BY"
}

{
	"id": 619762,
	"name": "Yakubovka",
	"country": "BY"

}

{
	"id": 619803,
	"name": "Vyzhary Vtoryye",
	"country": "BY"

}

{
	"id": 619805,
	"name": "Vyzhary Pervyye",
	"country": "BY"

}

{
	"id": 619818,
	"name": "Vysokaye",
	"country": "BY"

}

{
	"id": 619822,
	"name": "Vysokiye Lyady",
	"country": "BY"

}

{
	"id": 619917,
	"name": "Vsenezh’ye",
	"country": "BY"

}

{
	"id": 619955,
	"name": "Vostochnyy",
	"country": "BY"

}

{
	"id": 619979,
	"name": "Voranava",
	"country": "BY"
}

{
	"id": 620013,
	"name": "Volynka",
	"country": "BY"

}

{
	"id": 620015,
	"name": "Volya",
	"country": "BY"

}

{
	"id": 620079,
	"name": "Volkovichi",
	"country": "BY"

}

{
	"id": 620127,
	"name": "Vitebsk",
	"country": "BY"
}

{
	"id": 620134,
	"name": "Vitebsk Oblast",
	"country": "BY"
}

{
	"id": 620146,
	"name": "Vishnyeva",
	"country": "BY"
}

{
	"id": 620154,
	"name": "Vishevo",
	"country": "BY"

}

{
	"id": 620181,
	"name": "Vilyeyka",
	"country": "BY"
}

{
	"id": 620182,
	"name": "Vil’chytsy",
	"country": "BY"
}

{
	"id": 620205,
	"name": "Veyno",
	"country": "BY"

}

{
	"id": 620214,
	"name": "Vyetka",
	"country": "BY"

}

{
	"id": 620255,
	"name": "Vyerkhnyadzvinsk",
	"country": "BY"

}

{
	"id": 620335,
	"name": "Velikoye Selo",
	"country": "BY"
}

{
	"id": 620364,
	"name": "Velikaya Kurnitsa",
	"country": "BY"
}

{
	"id": 620391,
	"name": "Volkovysk",
	"country": "BY"
}

{
	"id": 620425,
	"name": "Vasilyevichy",
	"country": "BY"
}

{
	"id": 620445,
	"name": "Valozhyn",
	"country": "BY"

}

{
	"id": 620451,
	"name": "Valevka",
	"country": "BY"
}

{
	"id": 620453,
	"name": "Valer’yanovo",
	"country": "BY"

}

{
	"id": 620474,
	"name": "Uzhanka-Vel’ka",
	"country": "BY"
}

{
	"id": 620475,
	"name": "Uzhanka",
	"country": "BY"

}

{
	"id": 620483,
	"name": "Uzda",
	"country": "BY"
}

{
	"id": 620540,
	"name": "Ushachy",
	"country": "BY"
}

{
	"id": 620553,
	"name": "Uruch’ye",
	"country": "BY"

}

{
	"id": 620558,
	"name": "Urechcha",
	"country": "BY"
}

{
	"id": 620570,
	"name": "Ul’yanovka",
	"country": "BY"

}

{
	"id": 620643,
	"name": "Ubolot’ye",
	"country": "BY"

}

{
	"id": 620647,
	"name": "Tyukhinichi",
	"country": "BY"

}

{
	"id": 620676,
	"name": "Turaw",
	"country": "BY"

}

{
	"id": 620681,
	"name": "Turna Vyalikaya",
	"country": "BY"

}

{
	"id": 620692,
	"name": "Turets",
	"country": "BY"

}

{
	"id": 620712,
	"name": "Tudorovo",
	"country": "BY"
}

{
	"id": 620786,
	"name": "Troshchytsy",
	"country": "BY"

}

{
	"id": 620790,
	"name": "Trishin",
	"country": "BY"
}

{
	"id": 620833,
	"name": "Talachyn",
	"country": "BY"
}

{
	"id": 620851,
	"name": "Titovka",
	"country": "BY"

}

{
	"id": 620853,
	"name": "Tsishowka",
	"country": "BY"

}

{
	"id": 620869,
	"name": "Tsimkavichy",
	"country": "BY"
}

{
	"id": 620879,
	"name": "Teykovichi",
	"country": "BY"

}

{
	"id": 620945,
	"name": "Tsyelyakhany",
	"country": "BY"

}

{
	"id": 620955,
	"name": "Tatarsk",
	"country": "BY"
}

{
	"id": 620970,
	"name": "-",
	"country": "BY"
}

{
	"id": 621058,
	"name": "Svir",
	"country": "BY"
}

{
	"id": 621074,
	"name": "Svyetlahorsk",
	"country": "BY"
}

{
	"id": 621159,
	"name": "Sukhaya Grad’",
	"country": "BY"

}

{
	"id": 621235,
	"name": "Striyevka",
	"country": "BY"

}

{
	"id": 621243,
	"name": "Streshyn",
	"country": "BY"

}

{
	"id": 621255,
	"name": "Strazhevo",
	"country": "BY"
}

{
	"id": 621256,
	"name": "Strazhevichi",
	"country": "BY"

}

{
	"id": 621266,
	"name": "Stowbtsy",
	"country": "BY"
}

{
	"id": 621275,
	"name": "Stolovaya",
	"country": "BY"

}

{
	"id": 621277,
	"name": "Stolin",
	"country": "BY"
}

{
	"id": 621313,
	"name": "Stefanopol’",
	"country": "BY"
}

{
	"id": 621396,
	"name": "Staryya Darohi",
	"country": "BY"
}

{
	"id": 621420,
	"name": "Staroye Selo",
	"country": "BY"

}

{
	"id": 621462,
	"name": "Starobin",
	"country": "BY"
}

{
	"id": 621543,
	"name": "Stan’kava",
	"country": "BY"
}

{
	"id": 621548,
	"name": "Stanislavovo",
	"country": "BY"

}

{
	"id": 621563,
	"name": "Stakhovshchina",
	"country": "BY"

}

{
	"id": 621605,
	"name": "Sasnovy Bor",
	"country": "BY"

}

{
	"id": 621641,
	"name": "Soly",
	"country": "BY"
}

{
	"id": 621642,
	"name": "Saltanawshchyna",
	"country": "BY"

}

{
	"id": 621667,
	"name": "Sokolyanka",
	"country": "BY"

}

{
	"id": 621700,
	"name": "Snow",
	"country": "BY"
}

{
	"id": 621713,
	"name": "Smarhon’",
	"country": "BY"
}

{
	"id": 621719,
	"name": "Smolyanitsa",
	"country": "BY"

}

{
	"id": 621722,
	"name": "Smol’nitsa",
	"country": "BY"
}

{
	"id": 621728,
	"name": "Smalyavitski Rayon",
	"country": "BY",
	{
		"id": 621729,
		"name": "Horad Smalyavichy",
		"country": "BY"

	}

	{
		"id": 621732,
		"name": "Smilavichy",
		"country": "BY"

	}

	{
		"id": 621741,
		"name": "Slutsk",
		"country": "BY"
	}

	{
		"id": 621754,
		"name": "Slonim",
		"country": "BY"

	}

	{
		"id": 621769,
		"name": "Slobodka",
		"country": "BY"

	}

	{
		"id": 621779,
		"name": "Slabodka",
		"country": "BY"

	}

	{
		"id": 621817,
		"name": "Slabada",
		"country": "BY"

	}

	{
		"id": 621838,
		"name": "Slawharad",
		"country": "BY"
	}

	{
		"id": 621849,
		"name": "Slastsyeny",
		"country": "BY"
	}

	{
		"id": 621858,
		"name": "Skryshava",
		"country": "BY"
	}

	{
		"id": 621887,
		"name": "Skinderovka",
		"country": "BY"
	}

	{
		"id": 621903,
		"name": "Skabin",
		"country": "BY"
	}

	{
		"id": 621949,
		"name": "Simakovo",
		"country": "BY"

	}

	{
		"id": 621986,
		"name": "Shumilina",
		"country": "BY"

	}

	{
		"id": 622034,
		"name": "Shklow",
		"country": "BY"

	}

	{
		"id": 622044,
		"name": "Shyshchytsy",
		"country": "BY"

	}

	{
		"id": 622069,
		"name": "Sheypichi",
		"country": "BY"

	}

	{
		"id": 622096,
		"name": "Shani",
		"country": "BY"
	}

	{
		"id": 622113,
		"name": "Shchuchyn",
		"country": "BY"
	}

	{
		"id": 622125,
		"name": "Shchytkavichy",
		"country": "BY"

	}

	{
		"id": 622131,
		"name": "Shcharchova",
		"country": "BY"
	}

	{
		"id": 622179,
		"name": "Sharkawshchyna",
		"country": "BY"
	}

	{
		"id": 622206,
		"name": "Shakuny",
		"country": "BY"
	}

	{
		"id": 622228,
		"name": "Servech",
		"country": "BY"
	}

	{
		"id": 622245,
		"name": "Seredoma",
		"country": "BY"
	}

	{
		"id": 622258,
		"name": "Syanno",
		"country": "BY"
	}

	{
		"id": 622274,
		"name": "Syenitsa",
		"country": "BY"
	}

	{
		"id": 622279,
		"name": "Syomkava",
		"country": "BY"
	}

	{
		"id": 622341,
		"name": "Selets",
		"country": "BY"

	}

	{
		"id": 622420,
		"name": "Samakhvalavichy",
		"country": "BY"
	}

	{
		"id": 622428,
		"name": "Salihorsk",
		"country": "BY"
	}

	{
		"id": 622455,
		"name": "Ryzhkavichy",
		"country": "BY"

	}

	{
		"id": 622500,
		"name": "Ruzhany",
		"country": "BY"

	}

	{
		"id": 622575,
		"name": "Rudnya",
		"country": "BY"

	}

	{
		"id": 622606,
		"name": "Rutka 1",
		"country": "BY"
	}

	{
		"id": 622614,
		"name": "Rudzyensk",
		"country": "BY"

	}

	{
		"id": 622621,
		"name": "Rudawka",
		"country": "BY"
	}

	{
		"id": 622688,
		"name": "Rostkovo",
		"country": "BY"

	}

	{
		"id": 622690,
		"name": "Rasony",
		"country": "BY"
	}

	{
		"id": 622700,
		"name": "Ross’",
		"country": "BY"
	}

	{
		"id": 622739,
		"name": "Rahachow",
		"country": "BY"
	}

	{
		"id": 622794,
		"name": "Rechytsa",
		"country": "BY"
	}

	{
		"id": 622805,
		"name": "Rechitsa",
		"country": "BY"
	}

	{
		"id": 622811,
		"name": "Raytsa",
		"country": "BY"

	}

	{
		"id": 622816,
		"name": "Roviny",
		"country": "BY"

	}

	{
		"id": 622823,
		"name": "Ratamka",
		"country": "BY"
	}

	{
		"id": 622849,
		"name": "Rakaw",
		"country": "BY"
	}

	{
		"id": 622877,
		"name": "Radashkovichy",
		"country": "BY"
	}

	{
		"id": 622971,
		"name": "Pukhavichy",
		"country": "BY"
	}

	{
		"id": 622997,
		"name": "Pruzhany",
		"country": "BY"

	}

	{
		"id": 623109,
		"name": "Prigorod Kolozha",
		"country": "BY"

	}

	{
		"id": 623129,
		"name": "Prawdzinski",
		"country": "BY"

	}

	{
		"id": 623155,
		"name": "Patsyeyki",
		"country": "BY"

	}

	{
		"id": 623184,
		"name": "Posëlok Imeni Kalinina",
		"country": "BY"

	}

	{
		"id": 623208,
		"name": "Parechcha",
		"country": "BY"
	}

	{
		"id": 623227,
		"name": "Poplavy",
		"country": "BY"

	}

	{
		"id": 623228,
		"name": "Poplavshchina",
		"country": "BY"

	}

	{
		"id": 623255,
		"name": "Polykovichi-2",
		"country": "BY"

	}

	{
		"id": 623268,
		"name": "Paluzhzha",
		"country": "BY"

	}

	{
		"id": 623289,
		"name": "Vyalikaya Palonka",
		"country": "BY"

	}

	{
		"id": 623296,
		"name": "Polonaya",
		"country": "BY"

	}

	{
		"id": 623306,
		"name": "Polikarovka",
		"country": "BY"

	}

	{
		"id": 623317,
		"name": "Polatsk",
		"country": "BY"
	}

	{
		"id": 623407,
		"name": "Podlese",
		"country": "BY"

	}

	{
		"id": 623411,
		"name": "Podlese",
		"country": "BY"

	}

	{
		"id": 623440,
		"name": "Poddubno",
		"country": "BY"

	}

	{
		"id": 623465,
		"name": "Pobokovichi",
		"country": "BY" {
			"id": 623466,
			"name": "Pobokovichi",
			"country": "BY"

		}

		{
			"id": 623499,
			"name": "Pligovki Pervyye",
			"country": "BY"

		}

		{
			"id": 623512,
			"name": "Plyeshchanitsy",
			"country": "BY"
		}

		{
			"id": 623549,
			"name": "Pinsk",
			"country": "BY"
		}

		{
			"id": 623577,
			"name": "Pyatryshki",
			"country": "BY"

		}

		{
			"id": 623581,
			"name": "Pyetrykaw",
			"country": "BY"
		}

		{
			"id": 623588,
			"name": "Pyatralyevichy 1",
			"country": "BY"

		}

		{
			"id": 623596,
			"name": "Pesochnaya",
			"country": "BY"
		}

		{
			"id": 623641,
			"name": "Perunovo",
			"country": "BY"

		}

		{
			"id": 623649,
			"name": "Perkhovichi",
			"country": "BY" {
				"id": 623660,
				"name": "Perevoloka",
				"country": "BY"

			}

			{
				"id": 623678,
				"name": "Pyerasady",
				"country": "BY"

			}

			{
				"id": 623708,
				"name": "Pyelishcha",
				"country": "BY"

			}

			{
				"id": 623760,
				"name": "Pastavy",
				"country": "BY"
			}

			{
				"id": 623762,
				"name": "Pasinichi",
				"country": "BY"
			}

			{
				"id": 623788,
				"name": "Parychy",
				"country": "BY"

			}

			{
				"id": 623802,
				"name": "Papyernya",
				"country": "BY"

			}

			{
				"id": 623848,
				"name": "Ozginovichi",
				"country": "BY"

			}

			{
				"id": 623855,
				"name": "Azyartso",
				"country": "BY"

			}

			{
				"id": 623869,
				"name": "Ozerishche",
				"country": "BY"

			}

			{
				"id": 623902,
				"name": "Osveya",
				"country": "BY"

			}

			{
				"id": 623921,
				"name": "Ostrovok",
				"country": "BY"
			}

			{
				"id": 623925,
				"name": "Vostrava",
				"country": "BY"

			}

			{
				"id": 623934,
				"name": "Ostrovki",
				"country": "BY"
			}

			{
				"id": 623941,
				"name": "Astravyets",
				"country": "BY"
			}

			{
				"id": 623956,
				"name": "Astrashytski Haradok",
				"country": "BY"

			}

			{
				"id": 623985,
				"name": "Osovo",
				"country": "BY"
			}

			{
				"id": 623986,
				"name": "Vosava",
				"country": "BY"

			}

			{
				"id": 624034,
				"name": "Asipovichy",
				"country": "BY"

			}

			{
				"id": 624079,
				"name": "Orsha",
				"country": "BY"

			}

			{
				"id": 624089,
				"name": "Novoorlovichi",
				"country": "BY"

			}

			{
				"id": 624143,
				"name": "Oltush",
				"country": "BY"
			}

			{
				"id": 624147,
				"name": "Ol’shëvo",
				"country": "BY"

			}

			{
				"id": 624199,
				"name": "Aktsyabrski",
				"country": "BY"
			}

			{
				"id": 624265,
				"name": "Obukhovo",
				"country": "BY"
			}

			{
				"id": 624273,
				"name": "Abrova",
				"country": "BY"
			}

			{
				"id": 624297,
				"name": "Novy Svyerzhan’",
				"country": "BY"

			}

			{
				"id": 624318,
				"name": "Kadino",
				"country": "BY"

			}

			{
				"id": 624396,
				"name": "Novaya Vyoska",
				"country": "BY"

			}

			{
				"id": 624399,
				"name": "Novoye Mesto",
				"country": "BY"

			}

			{
				"id": 624400,
				"name": "Novoye Medvezhino",
				"country": "BY"

			}

			{
				"id": 624401,
				"name": "Novoyel’nya",
				"country": "BY"

			}

			{
				"id": 624418,
				"name": "Novosel’ye",
				"country": "BY"

			}

			{
				"id": 624453,
				"name": "Navasyolki",
				"country": "BY" {
					"id": 624457,
					"name": "Novosëlki",
					"country": "BY"

				}

				{
					"id": 624479,
					"name": "Novosady",
					"country": "BY"

				}

				{
					"id": 624494,
					"name": "Novolukoml’",
					"country": "BY"
				}

				{
					"id": 624535,
					"name": "Novyy Gorodok",
					"country": "BY"

				}

				{
					"id": 624538,
					"name": "Vysokaya Gorka",
					"country": "BY"

				}

				{
					"id": 624596,
					"name": "Novaya Derevnya",
					"country": "BY"

				}

				{
					"id": 624612,
					"name": "Nasilava",
					"country": "BY"

				}

				{
					"id": 624700,
					"name": "Nyasvizh",
					"country": "BY"

				}

				{
					"id": 624784,
					"name": "Navapolatsk",
					"country": "BY"

				}

				{
					"id": 624785,
					"name": "Novogrudok",
					"country": "BY"

				}

				{
					"id": 624799,
					"name": "Narowlya",
					"country": "BY"

				}

				{
					"id": 624804,
					"name": "Narach",
					"country": "BY"
				}

				{
					"id": 624805,
					"name": "Narach",
					"country": "BY"
				}

				{
					"id": 624812,
					"name": "Napalki",
					"country": "BY"
				}

				{
					"id": 624842,
					"name": "Myshkavichy",
					"country": "BY"

				}

				{
					"id": 624853,
					"name": "Myasata",
					"country": "BY"
				}

				{
					"id": 624861,
					"name": "Myadzyel",
					"country": "BY"
				}

				{
					"id": 624889,
					"name": "Mstsislaw",
					"country": "BY"
				}

				{
					"id": 624965,
					"name": "Mosar",
					"country": "BY"

				}

				{
					"id": 625027,
					"name": "Molochki",
					"country": "BY"
				}

				{
					"id": 625059,
					"name": "Moiseyevichi",
					"country": "BY"
				}

				{
					"id": 625066,
					"name": "Mogil’nitsy",
					"country": "BY"

				}

				{
					"id": 625067,
					"name": "Mogil’na",
					"country": "BY"

				}

				{
					"id": 625072,
					"name": "Mahilyowski Rayon",
					"country": "BY"
				}

				{
					"id": 625073,
					"name": "Mogilyov Oblast",
					"country": "BY" {
						"id": 625100,
						"name": "Mizhevichy",
						"country": "BY"

					}

					{
						"id": 625125,
						"name": "Miratichi",
						"country": "BY"

					}

					{
						"id": 625128,
						"name": "Mir",
						"country": "BY"

					}

					{
						"id": 625130,
						"name": "Myory",
						"country": "BY"
					}

					{
						"id": 625140,
						"name": "Minski Rayon",
						"country": "BY"
					}

					{
						"id": 625142,
						"name": "Minsk Oblast",
						"country": "BY" {
							"id": 625143,
							"name": "Horad Minsk",
							"country": "BY"

						}

						{
							"id": 625144,
							"name": "Minsk",
							"country": "BY"

						}

						{
							"id": 625228,
							"name": "Mikashevichy",
							"country": "BY"
						}

						{
							"id": 625229,
							"name": "Migovka",
							"country": "BY"
						}

						{
							"id": 625233,
							"name": "Mhlyo",
							"country": "BY"

						}

						{
							"id": 625234,
							"name": "Myazovichy",
							"country": "BY"

						}

						{
							"id": 625240,
							"name": "Malyy Mezhnik",
							"country": "BY"

						}

						{
							"id": 625245,
							"name": "Myazhysyatki",
							"country": "BY"

						}

						{
							"id": 625259,
							"name": "Mitropol’",
							"country": "BY"

						}

						{
							"id": 625281,
							"name": "Melyakhovichi",
							"country": "BY"
						}

						{
							"id": 625283,
							"name": "Mel’niki",
							"country": "BY"

						}

						{
							"id": 625298,
							"name": "Medenoye",
							"country": "BY"

						}

						{
							"id": 625324,
							"name": "Mazyr",
							"country": "BY"
						}

						{
							"id": 625367,
							"name": "Mosty",
							"country": "BY"
						}

						{
							"id": 625409,
							"name": "Mar’’ina Horka",
							"country": "BY"

						}

						{
							"id": 625495,
							"name": "Malyushychy",
							"country": "BY"
						}

						{
							"id": 625504,
							"name": "Malyavshchina",
							"country": "BY"
						}

						{
							"id": 625539,
							"name": "Malaryta",
							"country": "BY"

						}

						{
							"id": 625550,
							"name": "Malinovka",
							"country": "BY"
						}

						{
							"id": 625614,
							"name": "Mala-Uzhanka",
							"country": "BY"

						}

						{
							"id": 625625,
							"name": "Maladzyechna",
							"country": "BY"

						}

						{
							"id": 625642,
							"name": "Mokhovoye",
							"country": "BY"

						}

						{
							"id": 625665,
							"name": "Mahilyow",
							"country": "BY"
						}

						{
							"id": 625671,
							"name": "Machulishchy",
							"country": "BY"

						}

						{
							"id": 625712,
							"name": "Lyubcha",
							"country": "BY"

						}

						{
							"id": 625721,
							"name": "Lyuban’",
							"country": "BY"
						}

						{
							"id": 625737,
							"name": "Lyntupy",
							"country": "BY"
						}

						{
							"id": 625742,
							"name": "Lykovichi",
							"country": "BY"

						}

						{
							"id": 625743,
							"name": "Lyepyel’",
							"country": "BY"
						}

						{
							"id": 625772,
							"name": "Lyakhavichy",
							"country": "BY"
						}

						{
							"id": 625777,
							"name": "Lyady",
							"country": "BY"

						}

						{
							"id": 625818,
							"name": "Luninyets",
							"country": "BY"
						}

						{
							"id": 625853,
							"name": "Luhavaya Slabada",
							"country": "BY"

						}

						{
							"id": 625881,
							"name": "Lubnishche",
							"country": "BY"
						}

						{
							"id": 625907,
							"name": "Loyew",
							"country": "BY"
						}

						{
							"id": 625931,
							"name": "Loshnitsa",
							"country": "BY"
						}

						{
							"id": 625972,
							"name": "Lahoysk",
							"country": "BY"
						}

						{
							"id": 626005,
							"name": "Litovsk",
							"country": "BY"

						}

						{
							"id": 626034,
							"name": "Lipovka",
							"country": "BY"

						}

						{
							"id": 626041,
							"name": "Lipniški",
							"country": "BY"
						}

						{
							"id": 626050,
							"name": "Lipki",
							"country": "BY"

						}

						{
							"id": 626064,
							"name": "Lyozna",
							"country": "BY"
						}

						{
							"id": 626081,
							"name": "Lida",
							"country": "BY"

						}

						{
							"id": 626095,
							"name": "Lyawki",
							"country": "BY"

						}

						{
							"id": 626096,
							"name": "Levinova",
							"country": "BY"

						}

						{
							"id": 626188,
							"name": "Lyel’chytsy",
							"country": "BY"
						}

						{
							"id": 626226,
							"name": "Latygovka",
							"country": "BY"
						}

						{
							"id": 626238,
							"name": "Larinovka",
							"country": "BY"

						}

						{
							"id": 626266,
							"name": "Lahishyn",
							"country": "BY"
						}

						{
							"id": 626360,
							"name": "Kupely",
							"country": "BY"

						}

						{
							"id": 626418,
							"name": "Kudinovichi",
							"country": "BY"

						}

						{
							"id": 626450,
							"name": "Krychaw",
							"country": "BY"
						}

						{
							"id": 626462,
							"name": "Kruptsy",
							"country": "BY"
						}

						{
							"id": 626471,
							"name": "Krupki",
							"country": "BY"

						}

						{
							"id": 626478,
							"name": "Krupenino",
							"country": "BY"

						}

						{
							"id": 626491,
							"name": "Kruhlaye",
							"country": "BY"
						}

						{
							"id": 626523,
							"name": "Kryvichy",
							"country": "BY"

						}

						{
							"id": 626548,
							"name": "Kreva",
							"country": "BY"

						}

						{
							"id": 626579,
							"name": "Krasnyy Posëlok",
							"country": "BY"

						}

						{
							"id": 626610,
							"name": "Krasnaye",
							"country": "BY"
						}

						{
							"id": 626621,
							"name": "Krasnosel’skiy",
							"country": "BY"
						}

						{
							"id": 626625,
							"name": "Krasnapollye",
							"country": "BY"

						}

						{
							"id": 626633,
							"name": "Krasnitsa Vtoraya",
							"country": "BY"

						}

						{
							"id": 626634,
							"name": "Krasnitsa Pervaya",
							"country": "BY"
						}

						{
							"id": 626638,
							"name": "Krasnitsa",
							"country": "BY"

						}

						{
							"id": 626654,
							"name": "Krasnaya Sloboda",
							"country": "BY"

						}

						{
							"id": 626660,
							"name": "Chyrvonaya Slabada",
							"country": "BY"
						}

						{
							"id": 626698,
							"name": "Koz’yany",
							"country": "BY"

						}

						{
							"id": 626780,
							"name": "Kovali",
							"country": "BY"

						}

						{
							"id": 626799,
							"name": "Kotëlki",
							"country": "BY"
						}

						{
							"id": 626806,
							"name": "Kastsyukowka",
							"country": "BY"
						}

						{
							"id": 626808,
							"name": "Kastsyukovichy",
							"country": "BY"

						}

						{
							"id": 626826,
							"name": "Kastsyani",
							"country": "BY"

						}

						{
							"id": 626829,
							"name": "Kosava",
							"country": "BY"

						}

						{
							"id": 626849,
							"name": "Kashalyeva",
							"country": "BY"

						}

						{
							"id": 626861,
							"name": "Korzyuki",
							"country": "BY"
						}

						{
							"id": 626864,
							"name": "Karytnaye",
							"country": "BY"

						}

						{
							"id": 626884,
							"name": "Korostovo",
							"country": "BY"

						}

						{
							"id": 626895,
							"name": "Korolëv Stan",
							"country": "BY"

						}

						{
							"id": 626917,
							"name": "Karma",
							"country": "BY"
						}

						{
							"id": 626939,
							"name": "Karelichy",
							"country": "BY"

						}

						{
							"id": 626945,
							"name": "Karany",
							"country": "BY"
						}

						{
							"id": 626956,
							"name": "Kapyl’",
							"country": "BY"

						}

						{
							"id": 626986,
							"name": "Kontsy",
							"country": "BY"

						}

						{
							"id": 626992,
							"name": "Konstantinovo",
							"country": "BY"
						}

						{
							"id": 627057,
							"name": "Kolyadichi",
							"country": "BY"

						}

						{
							"id": 627083,
							"name": "Kalodzishchy",
							"country": "BY"
						}

						{
							"id": 627106,
							"name": "Kol’chitsy Novyye",
							"country": "BY"
						}

						{
							"id": 627121,
							"name": "Kokhonovka",
							"country": "BY"
						}

						{
							"id": 627123,
							"name": "Kokhanava",
							"country": "BY"

						}

						{
							"id": 627145,
							"name": "Kobryn",
							"country": "BY"
						}

						{
							"id": 627202,
							"name": "Klimavichy",
							"country": "BY"
						}

						{
							"id": 627205,
							"name": "Klichaw",
							"country": "BY"

						}

						{
							"id": 627214,
							"name": "Klyetsk",
							"country": "BY"

						}

						{
							"id": 627233,
							"name": "Klepachi",
							"country": "BY"
						}

						{
							"id": 627260,
							"name": "Kiseli",
							"country": "BY"
						}

						{
							"id": 627265,
							"name": "Kiselevichi",
							"country": "BY"
						}

						{
							"id": 627272,
							"name": "Kirawsk",
							"country": "BY"

						}

						{
							"id": 627318,
							"name": "Navinki",
							"country": "BY"

						}

						{
							"id": 627332,
							"name": "Khutor Apolinarovo",
							"country": "BY"

						}

						{
							"id": 627346,
							"name": "Khrol’chitsy",
							"country": "BY"
						}

						{
							"id": 627362,
							"name": "Khoyniki",
							"country": "BY"
						}

						{
							"id": 627403,
							"name": "Kharava",
							"country": "BY"

						}

						{
							"id": 627414,
							"name": "Khomichi",
							"country": "BY"

						}

						{
							"id": 627424,
							"name": "Kholopenichi",
							"country": "BY"

						}

						{
							"id": 627445,
							"name": "Kholbnya",
							"country": "BY"
						}

						{
							"id": 627470,
							"name": "Khmel’nitsa",
							"country": "BY"

						}

						{
							"id": 627490,
							"name": "Khimy",
							"country": "BY"

						}

						{
							"id": 627535,
							"name": "Gadabay Rayon",
							"country": "AZ",
						}

						{
							"id": 627631,
							"name": "Karabanovka",
							"country": "BY"

						}

						{
							"id": 627643,
							"name": "Kaplanovka",
							"country": "BY"

						}

						{
							"id": 627664,
							"name": "Kamyennyya Lavy",
							"country": "BY"

						}

						{
							"id": 627711,
							"name": "Kamyanyets",
							"country": "BY"

						}

						{
							"id": 627751,
							"name": "Kalinkavichy",
							"country": "BY"

						}

						{
							"id": 627773,
							"name": "Kachanovichy",
							"country": "BY"
						}

						{
							"id": 627793,
							"name": "Iwye",
							"country": "BY"

						}

						{
							"id": 627798,
							"name": "Ivyanyets",
							"country": "BY"
						}

						{
							"id": 627800,
							"name": "Ivatsevichy",
							"country": "BY"
						}

						{
							"id": 627811,
							"name": "Ivanava",
							"country": "BY"
						}

						{
							"id": 627878,
							"name": "Il’ya",
							"country": "BY"
						}

						{
							"id": 627904,
							"name": "Hrodna",
							"country": "BY"
						}

						{
							"id": 627905,
							"name": "Horki",
							"country": "BY"
						}

						{
							"id": 627907,
							"name": "Homyel'",
							"country": "BY"

						}

						{
							"id": 627908,
							"name": "Hlybokaye",
							"country": "BY"

						}

						{
							"id": 627909,
							"name": "Hantsavichy",
							"country": "BY"
						}

						{
							"id": 627950,
							"name": "Gusak",
							"country": "BY"
						}

						{
							"id": 627962,
							"name": "Guraki",
							"country": "BY"

						}

						{
							"id": 628021,
							"name": "Hrozava",
							"country": "BY"

						}

						{
							"id": 628035,
							"name": "Grodno Oblast",
							"country": "BY" {
								"id": 628051,
								"name": "Griskovshchina",
								"country": "BY"
							}

							{
								"id": 628079,
								"name": "Hresk",
								"country": "BY"

							}

							{
								"id": 628149,
								"name": "Gorokhovka",
								"country": "BY"
							}

							{
								"id": 628155,
								"name": "Haradok",
								"country": "BY"
							}

							{
								"id": 628175,
								"name": "Haradzishcha",
								"country": "BY"
							}

							{
								"id": 628182,
								"name": "Haradzyeya",
								"country": "BY"

							}

							{
								"id": 628204,
								"name": "Haradzyechna",
								"country": "BY"

							}

							{
								"id": 628213,
								"name": "Gornostaylovo",
								"country": "BY"

							}

							{
								"id": 628281,
								"name": "Homyel’ Voblasc’",
								"country": "BY" {
									"id": 628296,
									"name": "Golynka",
									"country": "BY"
								}

								{
									"id": 628317,
									"name": "Hal’shany",
									"country": "BY"
								}

								{
									"id": 628337,
									"name": "Holatsk",
									"country": "BY"

								}

								{
									"id": 628387,
									"name": "Hlusk",
									"country": "BY"
								}

								{
									"id": 628397,
									"name": "Hlusha",
									"country": "BY"
								}

								{
									"id": 628402,
									"name": "Glukhaya Seliba",
									"country": "BY"

								}

								{
									"id": 628511,
									"name": "Hatava",
									"country": "BY"

								}

								{
									"id": 628549,
									"name": "Gal’kovka",
									"country": "BY"

								}

								{
									"id": 628559,
									"name": "Galeyevka",
									"country": "BY"

								}

								{
									"id": 628579,
									"name": "Frolkovichi",
									"country": "BY"

								}

								{
									"id": 628634,
									"name": "Dzyarzhynsk",
									"country": "BY"
								}

								{
									"id": 628652,
									"name": "Dyudevo",
									"country": "BY"

								}

								{
									"id": 628654,
									"name": "Dymniki",
									"country": "BY"
								}

								{
									"id": 628658,
									"name": "Dyatlovo",
									"country": "BY"
								}

								{
									"id": 628684,
									"name": "Dvorishche",
									"country": "BY"
								}

								{
									"id": 628692,
									"name": "Dvorets",
									"country": "BY"

								}

								{
									"id": 628706,
									"name": "Dushkovtsy",
									"country": "BY"

								}

								{
									"id": 628727,
									"name": "Dukora",
									"country": "BY"
								}

								{
									"id": 628756,
									"name": "Dubrova",
									"country": "BY"
								}

								{
									"id": 628758,
									"name": "Dubrowna",
									"country": "BY"

								}

								{
									"id": 628763,
									"name": "Dubrovka",
									"country": "BY"

								}

								{
									"id": 628776,
									"name": "Dubrovka",
									"country": "BY"

								}

								{
									"id": 628816,
									"name": "Dubovlyany",
									"country": "BY"
								}

								{
									"id": 628865,
									"name": "Drybin",
									"country": "BY"

								}

								{
									"id": 628871,
									"name": "Druya",
									"country": "BY"

								}

								{
									"id": 628884,
									"name": "Drahichyn",
									"country": "BY"
								}

								{
									"id": 628897,
									"name": "Drychyn",
									"country": "BY"

								}

								{
									"id": 628923,
									"name": "Dowsk",
									"country": "BY"
								}

								{
									"id": 629002,
									"name": "Dokshytsy",
									"country": "BY"
								}

								{
									"id": 629009,
									"name": "Dabuchyn",
									"country": "BY"

								}

								{
									"id": 629018,
									"name": "Dobrush",
									"country": "BY"
								}

								{
									"id": 629055,
									"name": "Dzisna",
									"country": "BY"
								}

								{
									"id": 629056,
									"name": "Dinarovka",
									"country": "BY"

								}

								{
									"id": 629092,
									"name": "Dzyerawnaya",
									"country": "BY"

								}

								{
									"id": 629159,
									"name": "Davyd-Haradok",
									"country": "BY"

								}

								{
									"id": 629163,
									"name": "Dashkawka",
									"country": "BY"

								}

								{
									"id": 629183,
									"name": "Danilovichi",
									"country": "BY"
								}

								{
									"id": 629272,
									"name": "Chervino",
									"country": "BY"

								}

								{
									"id": 629273,
									"name": "Chervyen’",
									"country": "BY"

								}

								{
									"id": 629338,
									"name": "Charnawchytsy",
									"country": "BY"

								}

								{
									"id": 629347,
									"name": "Cherykaw",
									"country": "BY"
								}

								{
									"id": 629388,
									"name": "Chachevichy",
									"country": "BY"
								}

								{
									"id": 629390,
									"name": "Chachersk",
									"country": "BY"
								}

								{
									"id": 629395,
									"name": "Chavusy",
									"country": "BY"
								}

								{
									"id": 629400,
									"name": "Chashniki",
									"country": "BY"
								}

								{
									"id": 629447,
									"name": "Bykhaw",
									"country": "BY"

								}

								{
									"id": 629454,
									"name": "Byaroza",
									"country": "BY"
								}

								{
									"id": 629458,
									"name": "Belolozy",
									"country": "BY"
								}

								{
									"id": 629459,
									"name": "Byelahurna",
									"country": "BY"

								}

								{
									"id": 629471,
									"name": "Buynichy",
									"country": "BY"

								}

								{
									"id": 629516,
									"name": "Bukino",
									"country": "BY"
								}

								{
									"id": 629553,
									"name": "Budilovo",
									"country": "BY"

								}

								{
									"id": 629564,
									"name": "Buda-Kashalyova",
									"country": "BY"

								}

								{
									"id": 629589,
									"name": "Bryanchitsy",
									"country": "BY"
								}

								{
									"id": 629625,
									"name": "Bryli",
									"country": "BY"

								}

								{
									"id": 629631,
									"name": "Brest Oblast",
									"country": "BY" {
										"id": 629634,
										"name": "Brest",
										"country": "BY"

									}

									{
										"id": 629640,
										"name": "Braslaw",
										"country": "BY"
									}

									{
										"id": 629644,
										"name": "Brakovo",
										"country": "BY"

									}

									{
										"id": 629646,
										"name": "Brahin",
										"country": "BY"
									}

									{
										"id": 629663,
										"name": "Turets-Bayary",
										"country": "BY"
									}

									{
										"id": 629697,
										"name": "Baruny",
										"country": "BY"
									}

									{
										"id": 629736,
										"name": "Baravitsa",
										"country": "BY"

									}

									{
										"id": 629752,
										"name": "Baravaya",
										"country": "BY"

									}

									{
										"id": 629783,
										"name": "Borki",
										"country": "BY"

									}

									{
										"id": 629785,
										"name": "Borki",
										"country": "BY"

									}

									{
										"id": 629829,
										"name": "Vyaliki Trastsyanets",
										"country": "BY"
									}

									{
										"id": 629847,
										"name": "Velikoye Zaluzh’ye",
										"country": "BY"

									}

									{
										"id": 629858,
										"name": "Bol’shoye Savino",
										"country": "BY"

									}

									{
										"id": 629960,
										"name": "Velikaya Sloboda",
										"country": "BY"
									}

									{
										"id": 629962,
										"name": "Slepnya",
										"country": "BY"
									}

									{
										"id": 629986,
										"name": "Vyalikaya Byerastavitsa",
										"country": "BY"
									}

									{
										"id": 630070,
										"name": "Bobr",
										"country": "BY"
									}

									{
										"id": 630089,
										"name": "Blon’",
										"country": "BY"

									}

									{
										"id": 630131,
										"name": "Vyalikiya Basyady",
										"country": "BY"

									}

									{
										"id": 630136,
										"name": "Byeshankovichy",
										"country": "BY"
									}

									{
										"id": 630166,
										"name": "Byarozawka",
										"country": "BY"

									}

									{
										"id": 630197,
										"name": "Byerazino",
										"country": "BY"
									}

									{
										"id": 630202,
										"name": "Berezinka",
										"country": "BY"

									}

									{
										"id": 630206,
										"name": "Berëza",
										"country": "BY"
									}

									{
										"id": 630223,
										"name": "Berazinets",
										"country": "BY"
									}

									{
										"id": 630245,
										"name": "Byalynichy",
										"country": "BY"

									}

									{
										"id": 630279,
										"name": "Byelaazyorsk",
										"country": "BY"
									}

									{
										"id": 630336,
										"name": "Republic of Belarus",
										"country": "BY" {
											"id": 630343,
											"name": "Byahoml’",
											"country": "BY"

										}

										{
											"id": 630353,
											"name": "Bayki",
											"country": "BY"
										}

										{
											"id": 630376,
											"name": "Barysaw",
											"country": "BY"
										}

										{
											"id": 630391,
											"name": "Barsuki",
											"country": "BY"

										}

										{
											"id": 630428,
											"name": "Baranovichi",
											"country": "BY"

										}

										{
											"id": 630429,
											"name": "Baranovichi",
											"country": "BY"

										}

										{
											"id": 630431,
											"name": "Baran’",
											"country": "BY"

										}

										{
											"id": 630468,
											"name": "Babruysk",
											"country": "BY"

										}

										{
											"id": 630515,
											"name": "Ashmyany",
											"country": "BY"

										}

										{
											"id": 630543,
											"name": "Antopal’",
											"country": "BY"
										}

										{
											"id": 630630,
											"name": "Aleksandriya",
											"country": "BY"

										}