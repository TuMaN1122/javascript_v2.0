// Task 1 ============================================
/* Создайте функцию t1 которая записывает  в LS  ключ 5 со значением 11. Проверьте что происходит при повторном вызове функции. Запускается ф-я по кнопкуе b-1. */

function t1() {
    localStorage.setItem('5', 11);
    console.log(localStorage.getItem('5'));
}
document.querySelector('.b-1').onclick = t1;

// ваше событие здесь!!!

// Task 2 ============================================
/* Создайте функцию t2 которая записывает  в LS  массив a2 = [7,6,5]. Ключ a2. Проверьте что происходит при повторном вызове функции. Запускается ф-я по кнопкуе b-2. */
const a2 = [7, 6, 5];

function t2() {
    localStorage.setItem('a2', JSON.stringify(a2));
    console.log(localStorage.getItem('a2'));
}
document.querySelector('.b-2').onclick = t2;
// ваше событие здесь!!!


// Task 3 ============================================
/*  При нажатии кнопки t3 выведите из LS сохранненный массив a2. Выведите в out-3 в формате ключ пробел значение перенос строки.  */

function t3() {
    let b = localStorage.getItem('a2');
    b = JSON.parse(b);
    for (let i = 0; i < b.length; i++) {
        document.querySelector('.out-3').innerHTML += `${i} ${b[i]} <br>`;
    }
}
document.querySelector('.b-3').onclick = t3;

// ваше событие здесь!!!


// Task 4 ============================================
/*  Создайте функцию t4 которая записывает  в LS  массив a4 = {hello: world, hi:mahai}. Ключ a4. Проверьте что происходит при повторном вызове функции. Запускается ф-я по кнопкуе b-4.*/
const a4 = {
    hello: 'world',
    hi: 'mahai'
};

function t4() {
    localStorage.setItem('a4', JSON.stringify(a4));
    console.log(localStorage.getItem('a4'));
}
document.querySelector('.b-4').onclick = t4;
// ваше событие здесь!!!

// Task 5 ============================================
/*   При нажатии кнопки t5 выведите из LS сохранненный массив a24. Выведите в out-4 в формате ключ пробел значение перенос строки. */

function t5() {
    let b5 = localStorage.getItem('a4');
    b5 = JSON.parse(b5);
    for (let key in b5) {
        document.querySelector('.out-5').innerHTML += `${key} ${b5[key]} <br>`;
    }
}
document.querySelector('.b-5').onclick = t5;
// ваше событие здесь!!!

// Task 6 ============================================
/*  Создайте функцию t6 которая очищает весь LS. Запуск по кнопке b-6*/

function t6() {
    localStorage.clear();
}
document.querySelector('.b-6').onclick = t6;
// ваше событие здесь!!!


// Task 7 ============================================
/*  Создайте input i-7 куда пользователь может вводить числа и строки. Создайте массив a7. Когда пользователь нажимает кнопку b-7 число должно добавляться в массив. Массив должен сохраняться в LS с ключем a7.*/
let inp7 = document.querySelector('.i-7');
let a7 = [];

function t7() {
    a7.push(inp7.value);
    localStorage.setItem('a7', a7);
}
document.querySelector('.b-7').onclick = t7;
// ваше событие здесь!!!

// Task 8 ============================================
/*   Создайте функцию t8 при запуске которой из a7 удаляется последний элемент. После чего массив сохраняется в LS с ключем a7. Использовать массив из предыдущего задания. */

function t8() {
    a7.pop();
    localStorage.setItem('a7', a7);
}
document.querySelector('.b-8').onclick = t8;
// ваше событие здесь!!!


// Task 9 ============================================
/* Создайте 3 radiobutton c именем rb-9. Задайте для каждого value: #fff, #c0c0c0, #555. При изменении radibutton записывайте значение value в LS с ключем bg. Добавьте слушатель событий на изменение LS. Если есть ключ bg то при наступлении события изменять цвет фона на заданный в LS. */

let rb9 = document.querySelectorAll('input[name="rb-9"]');
let bg = document.querySelector('body');

function t9() {
    localStorage.setItem('bg', this.value);
    bg.style.backgroundColor = localStorage.getItem('bg');
}

for (let i = 0; i < rb9.length; i++) {
    rb9[i].onclick = t9;
}
// ваше событие здесь!!!


// Task 10 ============================================
/*  Проект. Дана переменная card - корзина. Добавьте кнопку b-10 и функцию t10, которые сохраняют card в LS.*/

// const card = {
//     'apple': 3,
//     'grape': 2
// }


// Task 11 ============================================
/*  Создайте фукнцию t11 которая читает корзину из LS и выводит на страницу в виде таблицы. Формат -  название товара - количество. Функция должна вызываться всегда после перезаписи LS ( в данном случае - просто добавьте ее вызов в нужные функции). */


// Task 12 ============================================
/*  Добавьте в таблицу кнопки плюс и минус возле каждого товара. При нажатии кнопки - изменяйте количество товаров в card, обновляйте LS, выводите на страницу. */

// Task 13 ============================================
/*  Добавьте в таблицу footer который считает общее количество товара. */


// Task 14 ============================================
/*  Добавьте функцию t13, которая при загрузке страницы проверяет наличие card в LS и если есть -выводит его на страницу. Если нет - пишет корзина пуста. */

/* const card = {
    'apple': 3,
    'grape': 2
};
const table = document.querySelector('table');

function t10() {
    localStorage.setItem('card', JSON.stringify(card));
    t11();
};

document.querySelector('.b-10').onclick = t10;

function t11() {
    function showTable() {
        const cardInfo = JSON.parse(localStorage.getItem('card'));
        let count = 0;
        table.innerHTML = '';

        if (cardInfo) {
            table.innerHTML += `
            <thead>
                <tr>
                    <th>Наименование</th>
                    <th>Количество</th>
                </tr>
                </thead>
                <tbody>`;
            for (let key in cardInfo) {
                table.innerHTML += `
                <tr>
                    <td>
                        <p><strong>${key}</strong></p>
                    </td>
                    <td>
                        <strong>${cardInfo[key]}</strong>
                    </td>
                    <td>
                        <p>
                        <button class="plus">+</button>
                        <button class="minus">-</button>
                        </p>
                    </td>
                </tr>
                </tbody>`;
                count += cardInfo[key];
            }
            table.innerHTML += `
                <tr>
                    <td>
                    <p>ОБЩЕЕ КОЛ-ВО ТОВАРОВ: ${count}</p>
                    </td>
                    <td>
                    </td>
                </tr>`;
        } else {
            table.innerHTML = '<h3">Корзина пуста</h3">'
        }
    }

    // Не удалось самостоятельно реализовать кнопки +/- , подсмотрел готовое решение у других участников

    function plusItem() {
        const plusBtn = document.querySelectorAll('.plus');
        const cardInfo = JSON.parse(localStorage.getItem('card'));
        plusBtn.forEach(btn => {
            btn.onclick = (e) => {
                let titleElem = e.target.parentNode.parentNode.parentNode.querySelector('tr td p').textContent;

                cardInfo[titleElem] += 1;
                localStorage.setItem('card', JSON.stringify(cardInfo));
                t11();
            };
        });
    }

    function minusItem() {
        const minusBtn = document.querySelectorAll('.minus');
        const cardInfo = JSON.parse(localStorage.getItem('card'));
        minusBtn.forEach(btn => {
            btn.onclick = (e) => {
                let titleElem = e.target.parentNode.parentNode.parentNode.querySelector('tr td p').textContent;
                if (cardInfo[titleElem] >= 2) {
                    cardInfo[titleElem] += -1;
                    localStorage.setItem('card', JSON.stringify(cardInfo));
                    t11();
                }

            };
        });
    }
    showTable();
    plusItem();
    minusItem();
}
t11(); */