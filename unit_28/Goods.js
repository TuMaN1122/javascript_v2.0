class Goods{
    constructor(options) {
        this.name = options.name;
        this.amount = options.amount;
        this.image = options.image;
        this.count = options.count;
    }
    draw() {
        let divWrap = document.createElement('div');
        let img = document.createElement('img');
        img.src = this.image;
        let p = document.createElement('p');
		p.innerHTML += `Наименование: ${this.name} / Цена: ${this.count}$`;
		divWrap.prepend(p);
		divWrap.append(img);
		return divWrap;
    }
}