class Valid {
    constructor(options) {
        this.email = options.email;
        this.password = options.password;
        this.isValid = options.isValid;
    }
    validate() {
        if (this.password.length > 6) {
            this.isValid = true;
            this.passwordError = '';
        } else {
            this.isValid = false;
            this.passwordError = 'min length 6';
        }
    }
}