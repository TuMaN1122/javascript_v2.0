class Goods {
	constructor(options) {
		this.name = options.name;
		this.amount = options.amount;
		this.image = options.image;
		this.count = options.count;
	}
	draw() {
		let divBox = document.createElement('div');
		divBox.classList.add('div-box');
		let img = document.createElement('img');
		img.classList.add('img-style');
		img.src = this.image;
		let tegP = document.createElement('p');
		tegP.classList.add('p-style');
		tegP.innerHTML += `Наименование: ${this.name} / Цена: ${this.count}$`;
		divBox.prepend(tegP);
		divBox.append(img);
		return divBox;
	}
}