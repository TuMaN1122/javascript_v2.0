class Valid2 extends Valid {
	constructor(options) {
		super(options);
		this.emaiError = '';
		this.passwordError = '';
	}
	validate() {
		super.validate();
		if (this.isValid === false) {
			return false;
		}
		if (this.email === '') {
			this.isValid = false;
			this.emaiError = 'email empty';
		}
		else {
			this.isValid = true;
			this.emaiError = '';
			this.passwordError = '';
		}
	}
}