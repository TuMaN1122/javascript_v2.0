// Task 1
// Создайте класс Goods. Класс должен содержать свойства name, amount. Создайте на его основе объект goods. Выведите в консоль созданный объект. Помните, все классы - в отдельных файлах. Имена классов с большой буквы.

const goods = new Goods({
	name: 'PlayStation-4 PRO',
	amount: 15,
	image: 'img/playstation.png',
	count: 400,
});

const goods2 = new Goods({
	name: 'PlayStation-5',
	amount: 5,
	image: 'img/playstation-5.png',
	count: 700,
});

const goods3 = new Goods2({
	name: 'контроллер PS5',
	amount: 3,
	image: 'img/dualsense.png',
	count: 300,
	sale: true,
});

const valid = new Valid({
	email: 'Danilaradyn3333@mail.ru',
	password: '12345',
});

valid.validate();
console.log(valid.isValid);


const copyValid = new Valid({
	email: 'Oleg@mail.ru',
	password: '123456778',
});

copyValid.validate();
console.log(copyValid.isValid);

const valid2 = new Valid2({
	email: '',
	password: '1234125',
	emaiError: '',
	passwordError: '',
});

valid2.validate();
console.log(valid2);

const valid3 = new Valid2({
	email: 'blablabla@mail.ru',
	password: '123412512',
	emaiError: '',
	passwordError: '',
});

valid3.validate();
console.log(valid3);

let ps4 = goods.draw();
let ps5 = goods2.draw();
let dualsense = goods3.draw();
document.querySelector('.out-3').append(ps4);
document.querySelector('.out-4').append(ps5);
document.querySelector('.out-6').append(dualsense);


 //Task 2.
 //  Добавьте в класс Goods свойства image и count - картинка и количество на складе.


 //Task 3.
 //  Добавьте в класс Goods метод draw, который будет выводить div с изображением, названием товара, ценой - в указанный элемент (дозапись).


 // Task 4.
 // Создайте на основе класса Goods объект goods2, заполните свойства, примените метод draw для вывода товара на страницу в блок out-4.


 //Task 5.
 // Создайте класс Goods2, который наследуется от Goods. Добавьте ему свойство sale равное true или false. Перезапишите метод draw так, чтобы он выводил информацию о распродажах.


 // Task 6.
 // Создайте на основе класса Goods2 объект goods3. Заполните все поля. Выведите товар на страницу с помощью метода draw. Вывод осуществить в out-6.


 // Task 7.
 // Создайте класс Valid, который содержит свойства email, password, isValid. И метод validate. Метод validate должен проверять длину пароля и писать false в isValid если длина меньше 6 и true если больше. Изначально свойство isValid равно false.


 //Task 8.
 // Создайте объект на основе класса Valid и задайте ему имя и пароль длиной 5 символов. Запустите метод validate() и выведите в консоль свойство isValid.


 //Task 9.
 // Создайте объект на основе класса Valid и задайте ему имя и пароль длиной 7 символов. Запустите метод validate() и выведите в консоль свойство isValid.


 //Task 10.
 // Унаследуйтесь от класса Valid и создайте класс Valid2. Расширьте его свойствами emaiError, passwordError. По умолчанию, они равны пустой строке. Перезапишите метод validate(), помимо проверки пароля, он должен содержать еще проверку свойства email на пустоту. Если поле email пустое - то isValid - false. Также, в случае ошибки валидации в поле emailError пишется сообщение ‘email empty’, в поле passwordError - ‘min length 6’.


 //Task 11.
 // Создайте на основе класса Valid2 объект valid2 и задайте пустой емейл и длину пароля больше 7. Запустите метод validate(). Выведите объект в консоль.


 //Task 12.
 // Создайте на основе класса Valid2 объект valid3 и задайте не пустой емейл и длину пароля больше 7. Запустите метод validate(). Выведите объект в консоль.
