class Goods2 extends Goods {
	constructor(options) {
		super(options);
		this.sale = options.sale;
	}
	draw() {
		const mainDiv = super.draw();
		if (this.sale) {
			let sale = document.createElement('div');
			sale.classList.add('sale');
			sale.innerHTML = 'Распродажа!!!';
			mainDiv.append(sale);
		}
		return mainDiv;
	}
}