
// Task 1 ============================================
/* 
 <p>Отправьте GET запрос на сайт http://getpost.itgid.info/index2.php. В качестве action укажите 1. </p>
<p>Отправьте GET запрос на сайт http://getpost.itgid.info/index2.php. В качестве action укажите 2. </p>
<p>Два запроса объедините с помощью promiseAll. Результат выведите в out-1 результат. Запускаться функция
    должна по нажатию b-1.</p>
*/

function t1() {
    let params1 = {
        action: 1,
    };

    let params2 = {
        action: 2,
    };

    createPromise('GET', url, '.out-1', params1, params2);    
}
document.querySelector('.b-1').onclick = t1;
// ваше событие здесь!!!

// Task 2 ============================================
/* 
 <p> Отправьте GET запрос на сайт http://getpost.itgid.info/index2.php. В качестве action укажите 3. Добавьте
параметр num1 и num2 содержащие числа. Если все сделано верно, сервер вернет сумму чисел.</p>
<p>Отправьте GET запрос на сайт http://getpost.itgid.info/index2.php. В качестве action укажите 4.
Добавьте параметр num1 и num2 содержащие числа. Если все сделано верно, сервер вернет случайное число в
заданном
диапазоне.</p>
<p>Два запроса объедините с помощью promiseAll.
Выведите в out-2 результат. Запускаться функция должна по нажатию b-2. </p>

*/

function t2() {
    let params1 = {
        action: 3,
        num1: 2,
        num2: 33
    };

    let params2 = {
        action: 4,
        num1: 2,
        num2: 33
    };

    createPromise('GET', url, '.out-2', params1, params2); 
    
}
document.querySelector('.b-2').onclick = t2;

// ваше событие здесь!!!


// Task 3 ============================================
/*  
<p> Отправьте GET запрос на сайт http://getpost.itgid.info/index2.php. В качестве action укажите 5.
Если все сделано верно, сервер вернет текущее время и дату. Не забывайте указывать параметр auth (ключ в
чате). </p>
<p> Отправьте GET запрос на сайт http://getpost.itgid.info/index2.php. В качестве action укажите 6.
Добавьте
параметр num1 и num2 содержащие числа. Если все сделано верно, сервер вернет большее число.</p>
<p>Два
запроса объедините с помощью promiseAll.
Выведите в out-3 результат. Запускаться функция должна по нажатию b-3. </p>
                 */

function t3() {
    let params1 = {
        action: 5
    };

    let params2 = {
        action: 6,
        num1: 2,
        num2: 33
    };

    createPromise('GET', url, '.out-3', params1, params2); 
    
}
document.querySelector('.b-3').onclick = t3;

// ваше событие здесь!!!


// Task 4 ============================================
/*  
 <p> Отправьте GET запрос на сайт http://getpost.itgid.info/index2.php. В качестве action укажите 7.
Если все
сделано верно, сервер случайную ссылку на изображение. Не забывайте указывать параметр auth (ключ в
чате). </p>
<p>Отправьте GET запрос на сайт http://getpost.itgid.info/index2.php. В качестве action укажите 8. В
качестве параметра по очереди укажите year равный году вашего рождения. Если все правильно сервер вернет
ваш возраст.</p>
<p>Выведите в out-4 результат. Запускаться функция должна по нажатию b-4.</p>

*/

function t4() {
    let params1 = {
        action: 7
    };

    let params2 = {
        action: 8,
        year: 1987
    };

    createPromise('GET', url, '.out-4', params1, params2); 
    
}
document.querySelector('.b-4').onclick = t4;

// ваше событие здесь!!!

// Task 5 ============================================
/*  
 <p>Отправьте POST запрос на сайт http://getpost.itgid.info/index2.php. В качестве action укажите 1.</p>
<p>Отправьте
POST запрос на сайт http://getpost.itgid.info/index2.php. В качестве action укажите 2. </p>
<p>Два
запроса объедините с помощью promiseAll. Результат выведите в out-5 результат. Запускаться функция
должна по нажатию b-5.</p>
*/

function t5() {
    let params1 = {
        action: 1
    };

    let params2 = {
        action: 2,
    };

    createPromise('POST', url, '.out-5', params1, params2); 
    
}
document.querySelector('.b-5').onclick = t5;

// ваше событие здесь!!!

// Task 6 ============================================
/* 
 <p> Отправьте POST запрос на сайт http://getpost.itgid.info/index2.php. В качестве action укажите 3.
    Добавьте
    параметр num1 и num2 содержащие числа. Если все сделано верно, сервер вернет сумму чисел. </p>
<p>Отправьте POST
    запрос на сайт http://getpost.itgid.info/index2.php. В качестве action укажите 4.
    Добавьте параметр num1 и num2 содержащие числа. Если все сделано верно, сервер вернет случайное число в
    заданном
    диапазоне.</p>
<p> Два запроса объедините с помощью promiseAll.
    Выведите в
    out-6 результат. Запускаться функция должна по нажатию b-6. </p>
*/

function t6() {
    let params1 = {
        action: 3,
        num1: 2,
        num2: 33
    };

    let params2 = {
        action: 4,
        num1: 2,
        num2: 33
    };

    createPromise('POST', url, '.out-6', params1, params2); 
    
}
document.querySelector('.b-6').onclick = t6;

// ваше событие здесь!!!


// Task 7 ============================================
/*  
 <p> Отправьте POST запрос на сайт http://getpost.itgid.info/index2.php. В качестве action укажите 5.
Если все сделано верно, сервер вернет текущее время и дату. Не забывайте указывать параметр auth (ключ в
чате).</p>
<p>Отправьте POST запрос на сайт http://getpost.itgid.info/index2.php. В качестве action укажите 6.
Добавьте параметр num1 и num2 содержащие числа. Если все сделано верно, сервер вернет большее число.</p>
<p>Два запроса объедините с помощью promiseAll.
Выведите в out-7 результат. Запускаться функция должна по нажатию b-7. </p>

*/

function t7() {
    let params1 = {
        action: 5
    };

    let params2 = {
        action: 6,
        num1: 2,
        num2: 33
    };

    createPromise('POST', url, '.out-7', params1, params2); 
    
}
document.querySelector('.b-7').onclick = t7;

// ваше событие здесь!!!

// Task 8 ============================================
/* 
<p> Отправьте POST запрос на сайт http://getpost.itgid.info/index2.php. В качестве action укажите 7.
Если все сделано верно, сервер случайную ссылку на изображение. Не забывайте указывать параметр auth
(ключ в
чате).</p>
<p>Отправьте POST запрос на сайт http://getpost.itgid.info/index2.php. В качестве action укажите 8. В
качестве параметра по очереди укажите year равный году вашего рождения. Если все правильно сервер вернет
ваш возраст.</p>
<p>Два запроса объедините с помощью promiseAll. Выведите в out-8 результат. Запускаться функция должна по
нажатию b-8.</p>
*/

function t8() {
    let params1 = {
        action: 7
    };

    let params2 = {
        action: 8,
        year: 1987
    };

    createPromise('POST', url, '.out-8', params1, params2); 
    
}
document.querySelector('.b-8').onclick = t8;

// ваше событие здесь!!!


const url = new URL('http://getpost.itgid.info/index2.php');
const AUTH = "DdC33D7d2C2a7";

function createPromise(method, url, out, params1, params2) {
    function getQueryString(obj) {
        let out = '';
        for (let key in obj) {
            out += '&' + key + '=' + obj[key];
        }
        return out;
    }
    let a = new Promise((resolve, reject) => {
        if (method === 'GET') {
            fetch(url + '?auth=' + AUTH + getQueryString(params1))
            .then(data => {
                resolve(data.text());
            })
        }
        if (method === 'POST') {
            fetch(url, {
                method: method,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
                body: 'auth=' + AUTH + getQueryString(params1),
            })
            .then(data => {
                resolve(data.text());
            })
        }
    
    });
    
    let b = new Promise((resolve, reject) => {
        if (method === 'GET') {
            fetch(url + '?auth=' + AUTH + getQueryString(params2))
            .then(data => {
                resolve(data.text());
            })
        }
        if (method === 'POST') {
            fetch(url, {
                method: method,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
                body: 'auth=' + AUTH + getQueryString(params2),
            })
            .then(data => {
                resolve(data.text());
            })
        }
    
    });
    
    Promise.all([a, b]).then(value => {
        document.querySelector(out).innerHTML = `${value[0]} <br> ${value[1]}`;
    });
}