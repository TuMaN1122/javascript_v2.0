/* ======== Task 1. =====
Создайте две переменные a = 7 и b = 9. Выведите в консоль результа умножения a на b. */

let a = 7,
    b = 9;
console.log(a * b);

/* ======== Task 2. ===== 
Создайте две переменные c = 7 и d = 9. Выведите на страницу результат деления c на d. */

let c = 7,
    d = 9;
document.querySelector('.unit-2').innerHTML = c / d;

/* ======== Task 3. ===== 
Создайте две переменные e = 3 и f = 5. Выведите на страницу результат сложения e + f. */

let e = 3,
    f = 5;
document.querySelector('.sum').innerHTML = e + f;



/* ======== Task 4. ===== 
Создайте две переменные e1 = '3' и f1 = 5. Выведите на страницу результат сложения e1 + f1. Объясните разницу. */

let e1 = '3',
    f1 = 5;
document.querySelector('.sum1').innerHTML = e1 + f1;



/* ======== Task 5. ===== 
Создайте две переменные e2 = 3 и f2 = 0. Выведите на страницу результат деления e2 на f2. */

let e2 = 3,
    f2 = 0;
document.querySelector('.division1').innerHTML = e2 / f2;



/* ======== Task 6. ===== 
Создайте две переменные e3 = 3 и f3 = 'Hello'. Выведите на страницу результат сложения двух переменных. */


let e3 = 3,
    f3 = 'Hello';
document.querySelector('.division2').innerHTML = e3 + f3;


/* ======== Task 7. ===== 
Создайте две переменные e4 = 3 и f4 = 'Hello'. Выведите на страницу результат умножения двух переменных. */

let e4 = 3,
    f4 = 'Hello';
document.querySelector('.multiplication').innerHTML = e4 * f4;



/* ======== Task 8. ===== 
Создайте input и кнопку. По нажатию на кнопку выполняется функция, которая выводит в консоль то, что пользователь ввел в input. */

let input8 = document.querySelector('.input-in'),
    out8 = document.querySelector('.out');
out8.onclick = function () {
    console.log(input8.value);
}




/* ======== Task 9. ===== 
Создайте input и кнопку. По нажатию на кнопку выполняется функция, которая выводит на страницу, что пользователь ввел в input. Добавьте очистку input после нажатия кнопки. */


let input9 = document.querySelector('.input-in-1'),
    out9 = document.querySelector('.out-1');
out9.onclick = function () {
    console.log(input9.value);
    input9.value = '';
}


/* ======== Task 10. ===== 
Создайте input и кнопку. По нажатию на кнопку выполняется функция, которая выводит на страницу число, которое ввел пользователь умноженное на 10. */

let input10 = document.querySelector('.input-in-2'),
    out10 = document.querySelector('.out-2');
out10.onclick = function () {
    console.log(input10.value * 10);
}



/* ======== Task 11. ===== 
Создайте input и кнопку. По нажатию на кнопку выполняется функция, которая выводит на страницу число, которое ввел пользователь и к нему добавленное число 10. */

let input11 = document.querySelector('.input-in-3'),
    out11 = document.querySelector('.out-3');
out11.onclick = function () {
    console.log(+input11.value + 10);
}



/* ======== Task 12. ===== 
Создайте два input и кнопку. В первый input пользователь вводит имя, во второе фамилию. При нажатии кнопки выполняется функция, которая выводит в консоль строку 'Hello имя фамилия', где имя - имя пользователя и фамилия - введенная фамилия. */

let firstName = document.querySelector('.first-name'),
    lastName = document.querySelector('.last-name'),
    out12 = document.querySelector('.out-4');
out12.onclick = function () {
    console.log('Hello ' + firstName.value + ' ' + lastName.value);
}



/* ======== Task 13. ===== 
Создайте два input и кнопку. В input пользователь вводит числа. При нажатии кнопки выполняется функция, которая выводит сумму данных двух чисел на страницу. */

let num1 = document.querySelector('.num-1'),
    num2 = document.querySelector('.num-2'),
    out13 = document.querySelector('.out-5');
out13.onclick = function () {
    document.querySelector('.input-value-5').innerHTML = +num1.value + +num2.value;
}



/* ======== Task 14. ===== 
Создайте input и пропишите ему в html value = 77. С помощью JS измените value на 'Hello'. */

let inputIn6 = document.querySelector('.input-in-6'),
    button6 = document.querySelector('.out-6');

button6.onclick = function () {
    inputIn6.value = 'Hello';
}



/* ======== Task 15. ===== 
Создайте input и получите его в переменную y. В js выполните следующее присвоение: y.style.border = '2px solid red' . Изучите результат операции. */


let y = document.querySelector('.input-in-7');
y.style.border = '2px solid red';



/* ======== Task 16. ===== 
Создайте два input type=number, куда пользователь может ввести числа. Выведите на страницу сумму данных чисел. */

let num3 = document.querySelector('.num-3'),
    num4 = document.querySelector('.num-4'),
    out16 = document.querySelector('.out-7');
out16.onclick = function () {
    document.querySelector('.input-value-7').innerHTML = +num3.value + +num4.value;
}



/* ======== Task 17. ===== 
Создайте input type="text" куда пользователь может ввести строку и число. Создайте функцию, которая запускается по нажатию на кнопку. Получите значение из input в переменную t. А затем сделайте операцию t = parseInt(t), и результат операции выведите в консоль. Прочитайте за эту операцию. Попробуйте по очереди вводить 44, 44aaa, 44.3, a45 . Изучите вывод. */

let input17 = document.querySelector('.task-17'),
    out17 = document.querySelector('.out-8');
out17.onclick = function () {
    let t = input17.value;
    console.log(t = parseInt(t));
}



/* ======== Task 18. ===== 
Создайте input type="text" куда пользователь может ввести строку и число. Создайте функцию, которая запускается по нажатию на кнопку. Получите значение из input в переменную t. А затем сделайте операцию t = parseFloat(t), и результат операции выведите в консоль. Прочитайте за эту операцию. Попробуйте по очереди вводить 44, 44aaa, 44.3, a45 . Изучите вывод. */

let input18 = document.querySelector('.task-18'),
    out18 = document.querySelector('.out-9');
out18.onclick = function () {
    let t = input18.value;
    console.log(t = parseFloat(t));
}



/* ======== Task ===== 19.
Создайте два input type=number, куда пользователь может ввести отрицательные числа. Выведите на страницу сумму данных чисел. Мы это делали! Зачем? Затем, что нужно понимать как влияет перевод в число с помощью + и parseInt на отрицательные числа!!! */



let num5 = document.querySelector('.num-5'),
    num6 = document.querySelector('.num-6'),
    inputValue10 = document.querySelector('.input-value-10'),
    button10 = document.querySelector('.out-10');

/* button6.onclick = function() {
    console.log(inputValue3);
    inputValue3.innerHTML = +num5.value + +num6.value;
} */
button10.onclick = function () {
    inputValue10.innerHTML = parseInt(num5.value) + parseInt(num6.value);
    console.log(+num5.value + +num6.value);
}



/* ======== Task ===== 20.
Создайте опросник, куда пользователь может ввести имя, фамилию, возраст, род занятий. И кнопку. По нажатию кнопки выведите на страницу предложение 'Уважаемый Иван, Иванов, ваш возраст 33 года, по професси вы ...' куда поставьте соответствующие данные из inputов. */
let firstName2 = document.querySelector('#first-name'),
    lastName2 = document.querySelector('#last-name'),
    age = document.querySelector('#age'),
    profession = document.querySelector('#profession'),
    answer = document.querySelector('#answer'),
    buttonAnswer = document.querySelector('#button-answer');

buttonAnswer.onclick = function () {
    answer.innerHTML = `Уважаемый ${firstName2.value} ${lastName2.value}, ваш возраст ${age.value} года, по професси вы ${profession.value}`;
}